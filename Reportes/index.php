

<?php
session_start();

if ($_SESSION['usuario'] == NULL) {
    ?><script>
            window.location = "../../Home/exit.php";
    </script><?php
}

$lvlroot = "../";
include_once($lvlroot . "Body/Head.php");
include_once($lvlroot . "Body/BeginPage.php");

include_once($lvlroot . "Body/SideBar.php");
?>



<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse " data-sortable-id="form-plugins-1" >
            <div class="panel-heading" style="background:#005b73;">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                </div>
                <h4 class="panel-title" >
                    <i class="fa fa-upload fa-lg"></i>
                    REPORTES DINÁMICOS           </h4>
            </div>

            <div class="panel-body panel-form " id="panel-output-form">


            </div>

            <br>

        </div>


    </div>
</div>



<?php
include_once($lvlroot . "Body/AlertsWindows.php");
?>
</div>
<!-- end row -->
<?php
// Including Js actions, put in the end.
include_once($lvlroot . "Body/JsFoot.php");
// Including End Header.
include_once($lvlroot . "Body/EndPage.php");

?>

<script>
    var lvlrootjs = <?php print json_encode($lvlroot); ?>;</script>

