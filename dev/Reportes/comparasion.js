var minatrasoayer;
var minatrasohoy;
var recorridoayer;
var recorridohoy;
function getyearmonthdaybefore()
{
    var today = new Date();

    var y = today.getFullYear();
    var d = today.getDate();
    var M = today.getMonth();
    return [y, M + 1, d - 1].join(':');
}

var ymdb = getyearmonthdaybefore();

function getyearmonthday()
{
    var today = new Date();

    var y = today.getFullYear();
    var d = today.getDate();
    var M = today.getMonth();
    return [y, M + 1, d].join(':');
}

var ymd = getyearmonthday();

var url3 = 'minatrasoayer.php';
$.getJSON(url3, function (result3) {
    minatrasoayer = result3;
});


var url2 = 'minatrasohoy.php';
$.getJSON(url2, function (result2) {
    minatrasohoy = result2;

});


var url4 = 'recorridoayer.php';
$.getJSON(url4, function (result4) {
    recorridoayer = result4;
});


var url5 = 'recorridohoy.php';
$.getJSON(url5, function (result5) {
    recorridohoy = result5;

});


FusionCharts.ready(function () {
    var fusioncharts = new FusionCharts({
        type: 'mscolumn2d',
        renderAt: 'chart-container',
        width: '700',
        height: '300',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "Minutos de Atraso - Tiempo de Recorrido ",
                "yAxisName": "Datos",
                "yAxisMinValue": "-14",
                "yAxisMaxValue": "10",

                "plotFillAlpha": "80",

                //Cosmetics
                "paletteColors": "#0075c2,#1aaf5d",
                "baseFontColor": "#333333",
                "baseFont": "Helvetica Neue,Arial",
                "captionFontSize": "14",
                "subcaptionFontSize": "14",
                "subcaptionFontBold": "0",
                "showBorder": "0",
                "bgColor": "#ffffff",
                "showShadow": "0",
                "canvasBgColor": "#ffffff",
                "canvasBorderAlpha": "0",
                "divlineAlpha": "100",
                "divlineColor": "#999999",
                "divlineThickness": "1",
                "divLineIsDashed": "1",
                "divLineDashLen": "1",
                "divLineGapLen": "1",
                "usePlotGradientColor": "0",
                "showplotborder": "0",
                "valueFontColor": "#666666",
                "placeValuesInside": "1",
                "showHoverEffect": "1",
                "rotateValues": "1",
                "showXAxisLine": "1",
                "xAxisLineThickness": "1",
                "xAxisLineColor": "#999999",
                "showAlternateHGridColor": "0",
                "legendBgAlpha": "0",
                "legendBorderAlpha": "0",
                "legendShadow": "0",
                "legendItemFontSize": "10",
                "legendItemFontColor": "#666666",
                  
                
            },
            "categories": [{
                    "category": [{
                            "label": "" + ymdb+ ""
                        }, {
                            "label": "" + ymd+ ""
                        }]
                }],
            "dataset": [{
                    "seriesname": "Minutos de Atraso",
                    "data": [{
                            "value": "" + minatrasoayer + ""
                        }, {
                            "value": "" + minatrasohoy + ""
                        }]
                }, {
                    "seriesname": "Tiempo de Recorrido",
                    "data": [{
                            "value": "" + recorridoayer + ""
                        }, {
                            "value": "" + recorridohoy + ""
                        }]
                }]

        }
      
    }
    );
    fusioncharts.render();
});
