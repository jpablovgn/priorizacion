

<script>
    function descargarArchivo() {
        var optionselected = $('#lista').val();
        var selected = optionselected.replace(/ /g, '');
        var fechaini = $('#fechainicial').val();
        var fechaFinal = $('#fechafinal').val();
        var strFirstThree = selected.substring(0, 3);

        switch (strFirstThree) {

            case 'Inf':
                var url23 = 'informeprioridades.php?fechaini=' + fechaini + '&fechafin=' + fechaFinal;
                $.getJSON(url23, function (result23) {
                    DownloadJSON2CSV3(result23);
                });
                break;
            case 'Aná':

                var url22 = 'analisisentreestaciones_1.php?fechaini=' + fechaini + '&fechafin=' + fechaFinal;
                $.getJSON(url22, function (result22) {
                    DownloadJSON2CSV2(result22);
                });
                break;

            case 'Reg':
                var url24 = 'registroprioridad_1.php?fechaini=' + fechaini + '&fechafin=' + fechaFinal;

                $.getJSON(url24, function (result24) {
                    DownloadJSON2CSV4(result24);

                });
                break;

            case 'Min':
                var url20 = 'returnreport.php?fechaini=' + fechaini + '&fechafin=' + fechaFinal;

                $.getJSON(url20, function (result20) {
                    DownloadJSON2CSV(result20);

                });
                break;
            case 'Tie':
                var url21 = 'tiemporecorridofechas.php?fechaini=' + fechaini + '&fechafin=' + fechaFinal;
                $.getJSON(url21, function (result21) {
                    DownloadJSON2CSV1(result21);

                });
            case 'Con':
                var url25 = 'reporteconteo.php?fechaini=' + fechaini + '&fechafin=' + fechaFinal;


                $.getJSON(url25, function (result25) {
                    DownloadJSON2CSV5(result25);

                });
        }
    }


    function DownloadJSON2CSV5(objArray)
    {
        var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
        var str = 'Intersección,Cantidad' + '\r\n';

        for (var i = 0; i < array.length; i++) {
            var line = '';
            for (var index in array[i]) {
                if (line != '')
                    line += ','

                line += array[i][index];
            }

            str += line + '\r\n';
        }

        if (navigator.appName != 'Microsoft Internet Explorer')
        {
            window.open('data:text/csv;charset=utf-8,' + escape(str));
        } else
        {
            var popup = window.open('', 'csv', '');
            popup.document.body.innerHTML = '<pre>' + str + '</pre>';
        }
    }

    function DownloadJSON2CSV(objArray)
    {
        var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
        var str = 'Bus,Prom_MinAtraso' + '\r\n';

        for (var i = 0; i < array.length; i++) {
            var line = '';
            for (var index in array[i]) {
                if (line != '')
                    line += ','

                line += array[i][index];
            }

            str += line + '\r\n';
        }

        if (navigator.appName != 'Microsoft Internet Explorer')
        {
            window.open('data:text/csv;charset=utf-8,' + escape(str));
        } else
        {
            var popup = window.open('', 'csv', '');
            popup.document.body.innerHTML = '<pre>' + str + '</pre>';
        }
    }

    function DownloadJSON2CSV1(objArray)
    {
        var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
        var str = 'Fecha,Hora,Bus,Tiempo_Recorrido' + '\r\n';

        for (var i = 0; i < array.length; i++) {
            var line = '';
            for (var index in array[i]) {
                if (line != '')
                    line += ','

                line += array[i][index];
            }

            str += line + '\r\n';
        }

        if (navigator.appName != 'Microsoft Internet Explorer')
        {
            window.open('data:text/csv;charset=utf-8,' + escape(str));
        } else
        {
            var popup = window.open('', 'csv', '');
            popup.document.body.innerHTML = '<pre>' + str + '</pre>';
        }
    }

    function DownloadJSON2CSV2(objArray)
    {
        var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
        var str = 'ip_origen,id_bus,ip_destino,min_atraso,tiempo_recorrido,fecha,hora' + '\r\n';

        for (var i = 0; i < array.length; i++) {
            var line = '';
            for (var index in array[i]) {
                if (line != '')
                    line += ','

                line += array[i][index];
            }

            str += line + '\r\n';
        }

        if (navigator.appName != 'Microsoft Internet Explorer')
        {
            window.open('data:text/csv;charset=utf-8,' + escape(str));
        } else
        {
            var popup = window.open('', 'csv', '');
            popup.document.body.innerHTML = '<pre>' + str + '</pre>';
        }
    }

    function DownloadJSON2CSV3(objArray)
    {
        var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
        var str = 'intersección,id_bus,solicito_prioridad,prioridad_aceptada,fecha,hora' + '\r\n';

        for (var i = 0; i < array.length; i++) {
            var line = '';
            for (var index in array[i]) {
                if (line != '')
                    line += ','

                line += array[i][index];
            }

            str += line + '\r\n';
        }

        if (navigator.appName != 'Microsoft Internet Explorer')
        {
            window.open('data:text/csv;charset=utf-8,' + escape(str));
        } else
        {
            var popup = window.open('', 'csv', '');
            popup.document.body.innerHTML = '<pre>' + str + '</pre>';
        }
    }


    function DownloadJSON2CSV4(objArray)
    {
        var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
        var str = 'fecha,8:00,9:00,10:00,11:00,12:00,01:00,02:00,03:00,04:00,05:00,06:00, porcentaje_hora,dia' + '\r\n';

        for (var i = 0; i < array.length; i++) {
            var line = '';
            for (var index in array[i]) {
                if (line != '')
                    line += ','

                line += array[i][index];
            }

            str += line + '\r\n';
        }

        if (navigator.appName != 'Microsoft Internet Explorer')
        {
            window.open('data:text/csv;charset=utf-8,' + escape(str));
        } else
        {
            var popup = window.open('', 'csv', '');
            popup.document.body.innerHTML = '<pre>' + str + '</pre>';
        }
    }
</script>

<?php
session_start();
if (isset($_GET['export']) && $_GET['export'] == '1') {
    download_file();
    exit();
}
if ($_SESSION['usuario'] == NULL) {
    ?><script>
            window.location = "../../Home/exit.php";
    </script><?php
}

$lvlroot = "../../";
include_once($lvlroot . "Body/Head.php");
include($lvlroot . "assets/css/datat.css");
include_once($lvlroot . "Body/BeginPage.php");

include_once($lvlroot . "Body/SideBar.php");

$url = 'http://192.168.50.187/spsa/heatmap.csv';

function download_file() {
    // getting conteoprioridadesnt to use in the example

    $content = file_get_contents($url);
    ?><?php
    $filename = '/tmp/reportebuses.csv';


    // creating temp file
    $handle = fopen($filename, 'w+');
    fwrite($handle, $content);
    fclose($handle);

    // setting headers
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename="' . basename($filename) . '"');
    header('Content-Length: ' . filesize($filename));
    return readfile($filename);
}
?>
<script src="http://cdn.leafletjs.com/leaflet/v0.7.7/leaflet.js"></script>
<script src="mapadecalor/dist/leaflet-heat.js"></script>
<script src="minatraso.js"></script>
<script src="fusioncharts.charts.js"></script>
<script src="fusioncharts.js"></script>
<script src="comparasion.js"></script>    

<script src="../../Home/container.js"></script>    

<script src="line.js"></script>
<script src="tme.js"></script>

<script>
        var lvlrootjs = <?php print json_encode($lvlroot); ?>;
        $(document).on('click', '#toggleAutoPlayBtn', function () {
            var fechainicial = $('#fechainicial').val();
            var fechafinal = $('#fechafinal').val();
            var url = "php/reportquery.php?fechainicial=" + fechainicial + "&fechafinal=" + fechafinal;
            $.getJSON(url, function (result) {

            });
        });
</script>

<script src="leaflet-heat.js"></script>

<!-- begin breadcrumb -->
<!--<ol class="breadcrumb pull-right">
    <li><a href="javascript:;">Inicio</a></li>
    <li><a href="javascript:;">Informes</a></li>
    <li class="active">Reportes</li>
</ol>-->
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse " data-sortable-id="form-plugins-1" >
            <div class="panel-heading" style="background:#005b73;">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                </div>
                <h4 class="panel-title" >
                    <i class="fa fa-upload fa-lg"></i>
                    TIEMPO ENTRE ESTACIONES             </h4>
            </div>
            <!-- begin body panel -->

            <div class="panel-body panel-form " id="panel-output-form">
                <form data-parsley-validate="true" id="formclient" name="formclient" class="form-horizontal form-bordered" method="post" action="">

                    <div class="form-group">


                        <div class="col-md-12"  >
                            <h3 style="color:#1a5b73;margin-left: 320px;">Informe Prioridades Solicitadas</h3>      

                            <br>
                            <table class="table table-bordered table-striped nowrap" id="prioridadaceptada" style="font-size: 12px;"class="display"width="100%;"></table>

                        </div>

                    </div>
                    <br><br>    
                    <div class="form-group">
                        <div class="col-md-12"  style="margin-left: -10px;margin-top: -71px;">
                            <br>
                            <h3 style="color:#1a5b73;margin-left: 320px;">Análisis entre Estaciones</h3>      

                            <br>
                            <table class="table table-bordered table-striped nowrap" id="analisisentreestaciones"style="font-size: 12px;" class="display"width="100%;"></table>

                        </div>

                    </div>
                    <div class="form-group">
                        <h4 style="color:#1a5b73;margin-left: 520px;">Prioridades Solicitadas por Estación</h4>      <br>


                        <div class="control-label col-md-2 col-sm-2 text-left">
                            <label>Fecha Inicial (*)</label>
                        </div>
                        <div class="col-md-2 col-sm-2">
                            <input type="date" name="fechainicial" id="fechainicial2" step="1" min="2013-01-01" max="2018-12-31" value="<?php echo date("Y-m-d"); ?>">

                        </div>

                        <div class="control-label col-md-2 col-sm-2 text-left">
                            <label>Fecha Final (*)</label>
                        </div>
                        <div class="col-md-2 col-sm-2">
                            <input type="date" name="fechafinal" id="fechafinal2" step="1" min="2013-01-01" max="2018-12-31" value="<?php echo date("Y-m-d"); ?>">

                        </div>

                    </div>
                    <script type="text/javascript" src="prioridadesestaciones.js"></script>
                    <div class="form-group col-md-12">

                        <div id="chart02-container"></div>
                        <div id='controllers'>
                            <input id='exportpdf' type='Button' value='Exportar a PDF' />&nbsp;&nbsp;
                            <input id='exportsvg' type='Button' value='Exportar a SVG' />&nbsp;&nbsp;
                            <input id='exportpng' type='Button' value='Exportar a PNG' />
                        </div>

                    </div>

            </div>

            </form>
            <br>

        </div>

        <script type="text/javascript">

            $(document).ready(function () {

                analisisentreestaciones();

            });


            function analisisentreestaciones() {

                var url = 'analisisentreestaciones.php';
                $.getJSON(url, function (result) {
                    if (result) {
                        analisisestaciones(result);
                    }
                });

                function analisisestaciones(result) {
                    $('#analisisentreestaciones').DataTable({
                        data: result,
                        destroy: true,

                        "language": {
                            "sProcessing": "Procesando...",
                            "sLengthMenu": "Mostrar _MENU_ registros",
                            "sZeroRecords": "No se encontraron resultados",
                            "sEmptyTable": "Ningún dato disponible en esta tabla",
                            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                            "sInfoPostFix": "",
                            "sSearch": "Buscar:",
                            "sUrl": "",
                            "sInfoThousands": ",",
                            "sLoadingRecords": "Cargando...",
                            "oPaginate": {
                                "sFirst": "Primero",
                                "sLast": "Último",
                                "sNext": "Siguiente",
                                "sPrevious": "Anterior"
                            },
                            "oAria": {
                                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                            }

                        },
                        columns: [
                            {title: "IP Origen"},
                            {title: "ID BUS"},
                            {title: "IP Destino"},
                            {title: "Minutos de Atraso"},
                            {title: "Tiempo de Recorrido"},
                            {title: "Fecha Hora"}



                        ]
                    });
                }


            }





        </script>

        <div class="panel panel-inverse " data-sortable-id="form-plugins-1" >
            <div class="panel-heading" style="background:#005b73;">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                </div>
                <h4 class="panel-title" >
                    <i class="fa fa-upload fa-lg"></i>
                    REPORTES             </h4>
            </div>
            <!-- begin body panel -->

            <div class="panel-body panel-form " id="panel-output-form">
                <form data-parsley-validate="true" id="formclient" name="formclient" class="form-horizontal form-bordered" method="post" action="">
                    <br>
                    <div class="form-group">
                        <div class="col-md-12"  style="margin-left: -10px;">

                            <h3 style="color:#1a5b73;margin-left: 320px;">Registro Prioridad Semafórica</h3>      

                            <br>
                            <table class="table table-bordered table-striped nowrap" id="registroprioridad"style="font-size: 12px;" class="display"width="100%;"></table>

                        </div>

                    </div>
                    <div class="form-group">
                        <div class="col-md-12"  style="margin-left: -10px;">

                            <h3 style="color:#1a5b73;margin-left: 320px;">Conteo Prioridades por Intersección</h3> <small>(Hoy)</small>     

                            <br>
                            <table class="table table-bordered table-striped nowrap" id="conteoprioridades"style="font-size: 12px;" class="display"width="100%;"></table>

                        </div>

                    </div>
                    <br>
                    <!--                                     <div class="form-group">
                                            <div class="col-md-12"  style="margin-left: -10px;">
                    
                                                <h3 style="color:#1a5b73;margin-left: 320px;">Conteo por Intersección en </h3>      
                    
                                                <br>
                                                <table class="table table-bordered table-striped nowrap" id="conteoprioridadesdinamico"style="font-size: 12px;" class="display"width="100%;"></table>
                    
                                            </div>
                    
                                                         </div><br>-->
                    <div class="form-group col-md-12" >
                        <h4 style="color:#1a5b73;margin-left: 520px;">Reportes Dinámicos</h4>      <br>


                        <div class="control-label col-md-2 col-sm-2 text-left">
                            <label>Fecha Inicial (*)</label>
                        </div>
                        <div class="col-md-2 col-sm-2">
                            <input type="date" name="fechainicial" id="fechainicial" step="1" min="2013-01-01" max="2018-12-31" value="<?php echo date("Y-m-d"); ?>">

                        </div>

                        <div class="control-label col-md-2 col-sm-2 text-left">
                            <label>Fecha Final (*)</label>
                        </div>
                        <div class="col-md-2 col-sm-2">
                            <input type="date" name="fechafinal" id="fechafinal" step="1" min="2013-01-01" max="2018-12-31" value="<?php echo date("Y-m-d"); ?>">

                        </div>
                        <div class="col-md-4 col-sm-4">
                            <select id="lista" class="form-control" >
                                <option>Informe prioridades solicitadas</option>
                                <option>Análisis entre estaciones</option>
                                <option>Registro Prioridad Semafórica</option>
                                <option>Conteo Prioridades por Controlador</option>
                                <option>Minutos de Atraso Por Bus - Promedio</option>
                                <option>Tiempo recorrido registrado</option>


                            </select>

                        </div>
                    </div>


                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>                    <div class="form-group">
                        <script type="text/javascript" src="https://cdn.datatables.net/r/dt/jq-2.1.4,jszip-2.5.0,pdfmake-0.1.18,dt-1.10.9,af-2.0.0,b-1.0.3,b-colvis-1.0.3,b-html5-1.0.3,b-print-1.0.3,se-1.0.1/datatables.min.js"></script>
                        <!--                        <div class="col-md-4 col-sm-4">
                                                                                <button type='button' id="generarreporte">Generar Reporte</button> 
                                                    <input type ="button" class="btn btn-success" id="toggleAutoPlayBtn" value="reporte"/>
                                                    <a href="?export=1">Exportar</a>
                        
                        
                                                </div>-->
                        <br><br>
                        <input type="button" class="btn-primary btn" style="padding: 6px 27px;margin-left: 10px;font-size: 15px;" value="Descargar" onclick=" descargarArchivo()"/>
                        <br>
<!--                        <script type="text/javascript" src="download.js"></script>
                        <div onClick="download()">
                            <i class="material-icons">file_download</i>
                            <p>DOWNLOAD CSV FILE</p>
                        </div>-->


                    </div>
                    <br>


                    </div>

                </form>
                <br>

            </div>

            <div class="panel panel-inverse " data-sortable-id="form-plugins-1">
                <div class="panel-heading" style="background:#005b73;">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                    <h4 class="panel-title">
                        <i class="fa fa-upload fa-lg"></i>
                        Minutos de Atraso y Tiempo Recorrido</div>
                <!-- begin body panel -->

                <div class="panel-body panel-form " id="panel-output-form">

                    <div class="form-group col-md-12">
                        <div class="col-md-5"><br>
                            <table class="table table-bordered table-striped nowrap" id="promedio_minatraso"style="font-size: 12px;" class="display" width="100%"></table>

                        </div>
                        <div class="col-md-6"><br>
                            <table class="table table-bordered table-striped nowrap" id="promedio_tiemporecorrido"style="font-size: 12px;" class="display" width="100%"></table>

                        </div>


                    </div>
                    <div class="form-group col-md-12">

                        <div class="col-md-6">
    <!--                        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
                            <script src="dist/leaflet-heat.js"></script>
    
    
                            <script>
            var url = 'heatquery.php';
            $.getJSON(url, function (result) {
                var addressPoints = result;
                var map = L.map('map').setView([6.2443382, -75.573553], 14);
                var tiles = L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
                    attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors',
                }).addTo(map);
                addressPoints = addressPoints.map(function (p) {
                    return [p[0], p[1]];
                });
                var heat = L.heatLayer(addressPoints, {radius: 25, max: 1410}).addTo(map);
            });
    
    </script>
                            <div id="map"></div>-->

                            <div id="chart-container"></div>
                            <!--                            <div id='controllers' style='position:relative;text-align:center'>
                                                            <input id='exportpdf' type='Button' value='Exportar a PDF' />&nbsp;&nbsp;
                                                            <input id='exportsvg' type='Button' value='Exportar a SVG' />&nbsp;&nbsp;
                                                            <input id='exportpng' type='Button' value='Exportar a PNG' />
                                                        </div>-->





                        </div>
                        <div class="col-md-5">




                        </div>

                    </div>

                    <div class="form-group col-md-12">
                        <div class="form-group col-md-6">   
                            <h4>Cantidad de Prioridades Solicitadas</h4>
                            <small>Hoy</small><br>

                            <script src="awesome/src/leaflet_awesome_number_markers.js"></script>
                            <div id="mapc" style="width: 550px; height: 450px"></div>
                            <script>
            var url36 = '5036.php';

//y            $.getJSON(url36, function (result36) {
//                myTimer(result36);
//            });
            var mapc = L.map('mapc').setView([6.2443382, -75.573553], 14);
            var tiles = L.tileLayer('https://cartodb-basemaps-{s}.global.ssl.fastly.net/dark_all/{z}/{x}/{y}.png', {
                attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors',
            }).addTo(mapc);

            var myVar = setInterval(myTimer, 5000);

            function myTimer() {
                var urldos = 'numeromapas.php';
                $.getJSON(urldos, function (resulttwo) {
                    cycle(resulttwo);
                });

                function cycle(resulttwo) {

                    var markers = new L.FeatureGroup();
                    for (var i = 0; i < resulttwo.length; i++) {
                        var color = pickRandomColor();
                        markers.addLayer(
                                new L.Marker([resulttwo[i][0], resulttwo[i][1]],
                                        {
                                            icon: new L.AwesomeNumberMarkers({number: resulttwo[i][2], markerColor: color})
                                        }
                                )
                                );
                    }
                    markers.addTo(mapc);
                }

                function pickRandomColor() {
                    var colors = ['red', 'darkred', 'orange', 'pink', 'green', 'darkgreen', 'blue', 'purple', 'darkpuple', 'cadetblue'];
                    return colors[Math.floor(Math.random() * colors.length)];
                }


            }


                            </script>


                        </div>
                        <div class="form-group col-md-6">
                            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
                            <script src="dist/leaflet-heat.js"></script>

        <!-- <script src="http://leaflet.github.io/Leaflet.markercluster/example/realworld.10000.js"></script> -->

                            <script>
            var url = 'heatquery.php';
            $.getJSON(url, function (result) {
                var addressPoints = result;
                var map = L.map('map').setView([6.2443382, -75.573553], 14);
                var tiles = L.tileLayer('https://cartodb-basemaps-{s}.global.ssl.fastly.net/dark_all/{z}/{x}/{y}.png', {
                    attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors',
                }).addTo(map);
                addressPoints = addressPoints.map(function (p) {
                    return [p[0], p[1]];
                });
                var heat = L.heatLayer(addressPoints, {radius: 25}).addTo(map);
            });
                            </script>
                            <h4>Sentido Aranjuez - UdeM</h4><br>
                            <div id="map" style="width:550px;height: 450px;"></div>

                        </div>


                    </div>
                </div>
            </div>



        </div>
    </div>

    <div class="form-group" id="cancel-save" style="display:none;">
        <div class="col-md-12 col-sm-12 text-right">
            <button id="cancelData" name="cancelData" class="btn btn-warning btn-sm">
                <i class="fa fa-ban">Cancelar</i>
            </button>
            <button id="save" name="save" class="btn btn-primary btn-sm">
                <i class="fa fa-floEndppy-o">
                    Guardar
                </i>
            </button>
        </div>
    </div>


    <?php
    include_once($lvlroot . "Body/AlertsWindows.php");
    ?>
</div>
<!-- end row -->
<?php
// Including Js actions, put in the end.
include_once($lvlroot . "Body/JsFoot.php");
// Including End Header.
include_once($lvlroot . "Body/EndPage.php");
// Writing on database.
//include_once("php/Insert2db.php");
?>

<script>
    var lvlrootjs = <?php print json_encode($lvlroot); ?>;</script>

<!-- // Loading autocomplete handler. -->
