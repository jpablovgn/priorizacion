


$(document).ready(function () {
    queryI();
    queryprioridad();
    prioridadaceptada();
    prioridadtiemporecorrido();
    analisisentreestaciones();
    conteoporinterseccion();


});
function queryI() {

    var url = 'registroprioridad.php';

    $.getJSON(url, function (result) {
        if (result) {
            tableUnits(result);
        }
    });
}

function tableUnits(result) {
    $(document).ready(function () {
        $('#registroprioridad').DataTable({
            data: result,
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ],

            "language": {
                "sProcessing": "Procesando...",
                "sLengthMenu": "Mostrar _MENU_ registros",
                "sZeroRecords": "No se encontraron resultados",
                "sEmptyTable": "Ningún dato disponible en esta tabla",
                "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix": "",
                "sSearch": "Buscar:",
                "sUrl": "",
                "sInfoThousands": ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst": "Primero",
                    "sLast": "Último",
                    "sNext": "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }

            },
            columns: [
                {title: "Fecha"},
                {title: "08:00"},
                {title: "09:00"},
                {title: "10:00"},
                {title: "11:00"},
                {title: "12:00"},
                {title: "13:00"},
                {title: "14:00"},
                {title: "15:00"},
                {title: "16:00"},
                {title: "Porcentaje Hora"},
                {title: "Día"}





            ]
        });
    });
}


function conteoporinterseccion() {

    var url01 = 'conteoporinterseccion.php';

    $.getJSON(url01, function (result01) {
        if (result01) {
            tableCount(result01);
        }
    });
}

function tableCount(result01) {
    $(document).ready(function () {
        $('#conteoprioridades').DataTable({
            data: result01,
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ],

            "language": {
                "sProcessing": "Procesando...",
                "sLengthMenu": "Mostrar _MENU_ registros",
                "sZeroRecords": "No se encontraron resultados",
                "sEmptyTable": "Ningún dato disponible en esta tabla",
                "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix": "",
                "sSearch": "Buscar:",
                "sUrl": "",
                "sInfoThousands": ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst": "Primero",
                    "sLast": "Último",
                    "sNext": "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }

            },
            columns: [
                {title: "Intersección"},
                {title: "Cantidad"}





            ]
        });
    });
}

function queryprioridad() {

    var url = 'prioridadaceptada.php';

    $.getJSON(url, function (result) {
        if (result) {
            tablepromedio(result);
        }
    });
}

function tablepromedio(result) {
    $(document).ready(function () {
        $('#prioridadaceptada').DataTable({

            data: result,

            "language": {
                "sProcessing": "Procesando...",
                "sLengthMenu": "Mostrar _MENU_ registros",
                "sZeroRecords": "No se encontraron resultados",
                "sEmptyTable": "Ningún dato disponible en esta tabla",
                "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix": "",
                "sSearch": "Buscar:",
                "sUrl": "",
                "sInfoThousands": ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst": "Primero",
                    "sLast": "Último",
                    "sNext": "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }

            },
            columns: [
                {title: "Semáforo"},
                {title: "BUS"},
                {title: "Prioridad"},
                {title: "Prioridad Aceptada"},
                {title: "Fecha"}





            ]
        });
    });
}


function prioridadaceptada() {
    var url = 'promedio_minatraso.php';
    $.getJSON(url, function (result) {
        if (result) {
            tablebusmin(result);
        }
    });

    function tablebusmin(result) {
        $('#promedio_minatraso').DataTable({
            data: result,
            "language": {
                "sProcessing": "Procesando...",
                "sLengthMenu": "Mostrar _MENU_ registros",
                "sZeroRecords": "No se encontraron resultados",
                "sEmptyTable": "Ningún dato disponible en esta tabla",
                "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix": "",
                "sSearch": "Buscar:",
                "sUrl": "",
                "sInfoThousands": ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst": "Primero",
                    "sLast": "Último",
                    "sNext": "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }

            },
            columns: [
                   {title: "Bus"},
                    {title: "Promedio Minutos de Atraso Semanal"}
                    


            ]
        });
    }

}

function prioridadtiemporecorrido() {
    var url = 'promedio_tiemporecorrido.php';
    $.getJSON(url, function (result) {
        if (result) {
            tablebustime(result);
        }
    });

    function tablebustime(result) {
        $('#promedio_tiemporecorrido').DataTable({
            data: result,
            "language": {
                "sProcessing": "Procesando...",
                "sLengthMenu": "Mostrar _MENU_ registros",
                "sZeroRecords": "No se encontraron resultados",
                "sEmptyTable": "Ningún dato disponible en esta tabla",
                "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix": "",
                "sSearch": "Buscar:",
                "sUrl": "",
                "sInfoThousands": ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst": "Primero",
                    "sLast": "Último",
                    "sNext": "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }

            },
            columns: [
                   {title: "Fecha"},
                    {title: "Bus"},
                    {title: "Tiempo Recorrido Registrado"}
                    
                    


            ]
        });
    }

}

function analisisentreestaciones(){
    
     var url = 'analisisentreestaciones.php';
    $.getJSON(url, function (result) {
        alert(result);
        if (result) {
            analisisestaciones(result);
        }
    });

    function analisisestaciones(result) {
        $('#analisisentreestaciones').DataTable({
            data: result,
            "language": {
                "sProcessing": "Procesando...",
                "sLengthMenu": "Mostrar _MENU_ registros",
                "sZeroRecords": "No se encontraron resultados",
                "sEmptyTable": "Ningún dato disponible en esta tabla",
                "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix": "",
                "sSearch": "Buscar:",
                "sUrl": "",
                "sInfoThousands": ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst": "Primero",
                    "sLast": "Último",
                    "sNext": "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }

            },
            columns: [
                   {title: "IP Origen"},
                    {title: "ID BUS"},
                    {title: "IP Destino"},
                    {title: "Minutos de Atraso"},
                    {title: "Fecha Hora"}
                    


            ]
        });
    }
    
    
}




