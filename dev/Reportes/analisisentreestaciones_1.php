

<?php

class Conexion {

    function conectar() {
        $conn = pg_connect("user=prioridad password=prioridad.2017 host=192.168.50.187 port=5432 dbname=sps");

        if (!$conn) {
            echo "Error, Problemas al conectar con el servidor";
            exit;
        } else {
            return $conn;
        }
    }

    function consulta($sql = null) {
        $resultado = pg_query(Conexion::conectar(), $sql);
        $fila = array();

        while ($row = pg_fetch_row($resultado)) {
            $fila[] = $row;
        }
        return $fila;
    }
    

}


$fechainicial = $_GET['fechaini'].' '.'00:00:00';
$fechafinal = $_GET['fechafin'].' '.'23:59:59';
    
    $result = Conexion::consulta("SELECT LAG(ip) OVER(ORDER BY fecha_hora desc) as ip_origen,id_bus,ip as ip_destino,min_atraso,tiempo_recorrido,fecha_hora FROM apps where fecha_hora between '$fechainicial' AND '$fechafinal' order by fecha_hora desc");

print json_encode($result);
