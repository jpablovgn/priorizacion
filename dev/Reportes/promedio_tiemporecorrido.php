

<?php

class Conexion {

    function conectar() {
        $conn = pg_connect("user=prioridad password=prioridad.2017 host=192.168.50.187 port=5432 dbname=sps");

        if (!$conn) {
            echo "Error, Problemas al conectar con el servidor";
            exit;
        } else {
            return $conn;
        }
    }

    function consulta($sql = null) {
        $resultado = pg_query(Conexion::conectar(), $sql);
        $fila = array();

        #Para obtener todos los datos debemos iterar en un ciclo, ya que contiene un puntero interno.
        while ($row = pg_fetch_row($resultado)) {
            $fila[] = $row;
        }
        return $fila;
    }

}

$fecha_inicial = '2017-03-06 00:00:00';
$fecha_final = '2017-03-10 23:59:59';

$Hour = date("H");
$Daybefore = date("d") -1;
$Month = date("m");
$Hourbefore = date("Y-$Month-$Daybefore H:i:s");
$Hournow = date("Y-m-d H:i:s");


$vble = "select fecha_hora,id_bus, tiempo_recorrido from apps where fecha_hora between '$Hourbefore' AND '$Hournow' AND id_bus SIMILAR TO 'P-%|A%'group by id_bus,fecha_hora,tiempo_recorrido order by fecha_hora desc;";
$result = Conexion::consulta($vble);


//$result["DATA"] = $result;

print json_encode($result);


