    
FusionCharts.ready(function() {
  var salesChart = new FusionCharts({
      type: 'MSarea',
      renderAt: 'chart21-container',
      width: '500',
      height: '375',
      dataFormat: 'json',
      dataSource: {
        "chart": {
          "caption": "Uso del Actuador Por Prioridad",
          "subcaption": "Buses Linea 1",
          "showvalues": "0",
          "captionpadding": "20",
          "numberPrefix": "",
          "formatnumberscale": "1",
          "showanchors": "0",
          "labeldisplay": "ROTATE",
          "yaxisvaluespadding": "10",
                      "yAxisMinValue":"70",

          "yaxismaxvalue": "100",
          "slantlabels": "1",
          "animation": "1",
          "paletteColors": "#b2d9f9,#f7c018,#94bf13,#ff9049,#069191",
          "divlinedashlen": "2",
          "divlinedashgap": "4",
          "divlineAlpha": "60",
          "drawCrossLine": "1",
          "crossLineColor": "#0d0d0d",
          "crossLineAlpha": "100",
          "crossLineAnimation": "1",
          "theme": "carbon",
                      "bgColor": "#FFFFFF"

        },
        "categories": [{
          "category": [{
            "label": "08:00"
          },  {
            "label": "10:00"
          }, {
            "label": "12:00"
          }, {
            "label": "02:00"
          }, {
            "label": "04:00"
          },  {
            "label": "06:00"
          }, {
            "label": "08:00"
          }]
        }],
        "dataset": [{
          "seriesname": "2017-03-06",
          "data": [{
            "value": "90"
          }, {
            "value": "76"
          }, {
            "value": "82"
          }, {
            "value": "93"
          }, {
            "value": "79"
          }, {
            "value": "87"
          }, {
            "value": "77"
          }]
        }, {
          "seriesname": "2017-03-07",
          "data": [{
            "value": "78"
          }, {
            "value": "92"
          }, {
            "value": "81"
          }, {
            "value": "76"
          }, {
            "value": "89"
          }, {
            "value": "77"
          }, {
            "value": "83"
          }]
        }]
      }
    })
    .render();
});
