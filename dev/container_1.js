FusionCharts.ready(function() {
  var salesChart = new FusionCharts({
      type: 'MSarea',
      renderAt: 'chart2-container',
      width: '500',
      height: '300',
      dataFormat: 'json',
      dataSource: {
        "chart": {
          "caption": "Uso de Estanterías Por Clientes ",
          "subcaption": "Abril 2016 - Marzo 2017",
          "showvalues": "0",
          "captionpadding": "20",
          "numberPrefix": "",
          "formatnumberscale": "1",
          "showanchors": "0",
          "labeldisplay": "ROTATE",
          "yaxisvaluespadding": "10",
          "yaxismaxvalue": "1000",
          "slantlabels": "1",
          "animation": "1",
          "paletteColors": "#b2d9f9,#f7c018,#94bf13,#ff9049,#069191",
          "divlinedashlen": "2",
          "divlinedashgap": "4",
          "divlineAlpha": "60",
          "drawCrossLine": "1",
          "crossLineColor": "#0d0d0d",
          "crossLineAlpha": "100",
          "crossLineAnimation": "1",
          "theme": "zune"
        },
        "categories": [{
          "category": [{
            "label": "Apr'16"
          }, {
            "label": "May'16"
          }, {
            "label": "Jun'16"
          }, {
            "label": "Jul'16"
          }, {
            "label": "Aug'16"
          }, {
            "label": "Sep'16"
          }, {
            "label": "Oct'16"
          }, {
            "label": "Nov'16"
          }, {
            "label": "Dec'16"
          }, {
            "label": "Jan'17"
          }, {
            "label": "Feb'17"
          }, {
            "label": "Mar'17"
          }]
        }],
        "dataset": [{
          "seriesname": "Tennis",
          "data": [{
            "value": "250"
          }, {
            "value": "242"
          }, {
            "value": "330"
          }, {
            "value": "241"
          }, {
            "value": "119"
          }, {
            "value": "550"
          }, {
            "value": "220"
          }, {
            "value": "215"
          }, {
            "value": "215"
          }, {
            "value": "220"
          }, {
            "value": "115"
          }, {
            "value": "593"
          }]
        }, {
          "seriesname": "Vinos & Vinos",
          "data": [{
            "value": "186"
          }, {
            "value": "173"
          }, {
            "value": "236"
          }, {
            "value": "155"
          }, {
            "value": "461"
          }, {
            "value": "392"
          }, {
            "value": "139"
          }, {
            "value": "129"
          }, {
            "value": "144"
          }, {
            "value": "120"
          }, {
            "value": "469"
          }, {
            "value": "386"
          }]
        }, {
          "seriesname": "Frutos y Semillas",
          "data": [{
            "value": "132"
          }, {
            "value": "138"
          }, {
            "value": "140"
          }, {
            "value": "130"
          }, {
            "value": "85"
          }, {
            "value": "284"
          }, {
            "value": "131"
          }, {
            "value": "110"
          }, {
            "value": "414"
          }, {
            "value": "115"
          }, {
            "value": "538"
          }, {
            "value": "304"
          }]
        }, {
          "seriesname": "Indurrajes",
          "data": [{
            "value": "114"
          }, {
            "value": "115"
          }, {
            "value": "157"
          }, {
            "value": "691"
          }, {
            "value": "315"
          }, {
            "value": "284"
          }, {
            "value": "103"
          }, {
            "value": "180"
          }, {
            "value": "377"
          }, {
            "value": "107"
          }, {
            "value": "463"
          }, {
            "value": "284"
          }]
        }, {
          "seriesname": "Cotton Blue",
          "data": [{
            "value": "41"
          }, {
            "value": "545"
          }, {
            "value": "665"
          }, {
            "value": "54"
          }, {
            "value": "173"
          }, {
            "value": "151"
          }, {
            "value": "45"
          }, {
            "value": "584"
          }, {
            "value": "700"
          }, {
            "value": "114"
          }, {
            "value": "194"
          }, {
            "value": "478"
          }]
        }]
      }
    })
    .render();
});
