<?php
// The lvlroot variable indicates the levels of direcctories
// the file loaded has to up, to be on the root directory
$lvlroot = "../";
// Including Head.
include_once($lvlroot . "Body/Head.php");
include($lvlroot . "assets/css/datat.css");
// Including Begin Header.
include_once($lvlroot . "Body/BeginPage.php");
//
// Including Side bar.
include_once($lvlroot . "Body/SideBar.php");
// Including Php database.
include_once($lvlroot . "assets/php/PhpMySQL.php");
// function defined in js/autocomplete.js
$nosubmit = "if (event.keyCode == 13) event.preventDefault()";
$autoCompleteCall = "if (event.keyCode == 13) { autoCompleteOthers(this); event.preventDefault(); }";
// Prevent default post
$preventDefault = "if (event.keyCode == 13) { event.preventDefault(); }";
//unset($ingreso,$destino,$importacion);
//var_dump($ingreso,$destino,$importacion);
unset($resultado);
?> 
<script src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="http://code.jquery.com/jquery-1.8.2.min.js"></script>
<script src="http://cdn.oesmith.co.uk/morris-0.4.1.min.js"></script>
<script src="fusioncharts.js"></script>
<script src="3d.js"></script>
<script src="container.js"></script>
<script src="line.js"></script>
<script src="circle.js"></script>
<script src="leaflet-heatmap.js"></script>


<script>
    var lvlrootjs = <?php print json_encode($lvlroot); ?>;
</script>
<!-- begin breadcrumb -->
<!--<ol class="breadcrumb pull-right">
    <li><a href="javascript:;">Inicio</a></li>
    <li><a href="javascript:;">Proceso Bodega</a></li>
    <li class="active">Selección de Unidades</li>
</ol>-->
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse " data-sortable-id="form-plugins-1">
            <div class="panel-heading" style="background: #023141;">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                </div>
                <h4 class="panel-title">
                    <i class="fa fa-upload fa-lg"></i>
                    Corredores Activos                      </h4>
            </div>
            <!-- begin body panel -->
            <div class="panel-body panel-form " id="panel-output-form">
                <form data-parsley-validate="true" id="formclient" name="formclient" class="form-horizontal form-bordered" method="post" action="">
                    <div class="form-group col-md-12">
                        <div class="col-md-9">
                            <div id="map" style="width:800px;height:800px;"></div>
                            <script>

//                                var map = L.map('map').
//                                        setView([6.2443382, -75.573553
//                                        ], 14);
//                                map.on('click', function (e) {
//                                    alert("Lat, Lon : " + e.latlng.lat + ", " + e.latlng.lng);
//                                });
//
//                                L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
//                                    attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://cloudmade.com">CloudMade</a>',
//                                    maxZoom: 18
//                                }).addTo(map);
//
//                                L.control.scale().addTo(map);
//                                L.marker([6.2443382, -75.573553], {draggable: true}).addTo(map);

// don't forget to include leaflet-heatmap.js
                                var testData = {
                                    max: 8,
                                    data: [{lat: 24.6408, lng: 46.7728, count: 3}, {lat: 50.75, lng: -1.55, count: 1}, ...]
                                };

                                var baseLayer = L.tileLayer(
                                        'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                                            attribution: '...',
                                            maxZoom: 18
                                        }
                                );

                                var cfg = {
                                    // radius should be small ONLY if scaleRadius is true (or small radius is intended)
                                    // if scaleRadius is false it will be the constant radius used in pixels
                                    "radius": 2,
                                    "maxOpacity": .8,
                                    // scales the radius based on map zoom
                                    "scaleRadius": true,
                                    // if set to false the heatmap uses the global maximum for colorization
                                    // if activated: uses the data maximum within the current map boundaries 
                                    //   (there will always be a red spot with useLocalExtremas true)
                                    "useLocalExtrema": true,
                                    // which field name in your data represents the latitude - default "lat"
                                    latField: 'lat',
                                    // which field name in your data represents the longitude - default "lng"
                                    lngField: 'lng',
                                    // which field name in your data represents the data value - default "value"
                                    valueField: 'count'
                                };


                                var heatmapLayer = new HeatmapOverlay(cfg);

                                var map = new L.Map('map-canvas', {
                                    center: new L.LatLng(25.6586, -80.3568),
                                    zoom: 4,
                                    layers: [baseLayer, heatmapLayer]
                                });

                                heatmapLayer.setData(testData);

                            </script>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group" style="align-content: center">

                                <h4 style="margin-left:163px;">Ruta<small> </small></h4>
                            </div>


                            <select class="form-control" id="select-required" name="perfil" data-parsley-required="true">

                                <option value="" selected >Seleccione una Ruta:</option>
                                <?php
//                                $perfiles = "CALL CONSULTAR_PERFILES()";
//                                $queryResult = $Client->query($perfiles);
//                                while ($clientData = $Client->fetch_array_assoc($queryResult)) {
//                                    
                                ?>
                                <option value="//<?php echo $clientData['ID_PERFIL'] ?>"  <?php echo $clientData['ID_PERFIL'] == $campos['perfil'] ? "selected" : "" ?>><?php echo $clientData['PERFIL'] ?></option>
                                //<?PHP
//                                }
//                                $Client->close();
                                ?>
                            </select><br>
                            <div id="chart3-container"></div>
                            <br>
                            <table class="table" style="margin-left: 32px; width:90%;">
                                <tr style="border:1px; height: 3px;" >
                                    <td style="background:#4a9de8;">
                                        <label class="control-label  text-center" for="message" style="color:#ffffff;">% Buses que reportan en geocercas</label>
                                    </td>
                                    <td>
                                        <label class="control-label  text-center" id="buses" for="message"></label>

                                    </td>
                                </tr>
                                <tr style="border:1px;" >
                                    <td style="background:#4a9de8;">
                                        <label class="control-label  text-center" for="message" style="color:#ffffff;">% Geocercas que detecaron buses</label>
                                    </td>
                                    <td>
                                        <label class="control-label  text-center" for="message" > 20</label>

                                    </td>
                                </tr>
                                <tr style="border:1px;" >
                                    <td style="background:#4a9de8;">
                                        <label class="control-label  text-center" for="message" style="color:#ffffff;"># Geocercas que detectaron prioridad</label>
                                    </td>
                                    <td>
                                        <label class="control-label  text-center" for="message" > 13</label>

                                    </td>
                                </tr>                               <tr style="border:1px;" >
                                    <td style="background:#4a9de8;">
                                        <label class="control-label  text-center" for="message" style="color:#ffffff;"># Intersecciones que no registraron buses</label>
                                    </td>
                                    <td>
                                        <label class="control-label  text-center" for="message" > 21</label>

                                    </td>
                                </tr>
                                <tr style="border:1px;" >
                                    <td style="background:#4a9de8;">
                                        <label class="control-label  text-center" for="message" style="color:#ffffff;"># Buses que no solicitan prioridad</label>
                                    </td>
                                    <td>
                                        <label class="control-label  text-center" for="message" > 7</label>

                                    </td>
                                </tr>
                                <tr style="border:1px;" >
                                    <td style="background:#4a9de8;">
                                        <label class="control-label  text-center" for="message" style="color:#ffffff;"># Buses Inactivos</label>
                                    </td>
                                    <td>
                                        <label class="control-label  text-center" for="message" > 14</label>

                                    </td>
                                </tr>
                            </table>
                            <div class="form-group" style="align-content: center">

                                <h4 style="margin-left:163px;">Geocerca<small> </small></h4>
                                <select class="form-control" id="select-required" name="perfil" data-parsley-required="true">

                                    <option value="" selected >Seleccione una Ruta:</option>
                                    <?php
//                                $perfiles = "CALL CONSULTAR_PERFILES()";
//                                $queryResult = $Client->query($perfiles);
//                                while ($clientData = $Client->fetch_array_assoc($queryResult)) {
//                                    
                                    ?>
                                    <option value="//<?php echo $clientData['ID_PERFIL'] ?>"  <?php echo $clientData['ID_PERFIL'] == $campos['perfil'] ? "selected" : "" ?>><?php echo $clientData['PERFIL'] ?></option>
                                    //<?PHP
//                                }
//                                $Client->close();
                                    ?>
                                </select><br>
                            </div>                    <div id="chart4-container"></div>


                        </div>
                    </div>


                </form>
                <br>
            </div>
        </div>



    </div>
</div>
<script type="text/javascript">
    jQuery(function() {
        jQuery('td#cashcalculator_total a').html('XXX');
    });
</script>
<div class="form-group" id="cancel-save" style="display:none;">
    <div class="col-md-12 col-sm-12 text-right">
        <button id="cancelData" name="cancelData" class="btn btn-warning btn-sm">
            <i class="fa fa-ban">Cancelar</i>
        </button>
        <button id="save" name="save" class="btn btn-primary btn-sm">
            <i class="fa fa-floEndppy-o">
                Guardar
            </i>
        </button>
    </div>
</div>


<?php
include_once($lvlroot . "Body/AlertsWindows.php");
?>
</div>
<!-- end row -->
<?php
// Including Js actions, put in the end.
include_once($lvlroot . "Body/JsFoot.php");
// Including End Header.
include_once($lvlroot . "Body/EndPage.php");
// Writing on database.
//include_once("php/Insert2db.php");
?>

<script>
    var lvlrootjs = <?php print json_encode($lvlroot); ?>;
</script>

<!-- // Loading autocomplete handler. -->

<script type="text/javascript" src="js/setValidations.js"></script>
<!-- // Loading validation file. -->
<!--<script type="text/javascript" src="js/validationBatch.js"></script>
<script type="text/javascript">
        //$('#Batch-table').DataTable();
        // Configuring fileupload
        $(function(){
                $('#fileupload').fileupload({
                        maxFileSize: 5242880,
                        maxNumberOfFiles: 1,
                        acceptFileTypes:/(\.|\/)(csv)$/i
                });
        });
</script>-->
<script type="text/javascript">
    // Activating the side bar.
    var Change2Activejs = document.getElementById("sidebarProcesoBodega");
    Change2Activejs.className = "has-sub active";
    var Change2Activejs = document.getElementById("sidebarProcesoBodega-BatchPicking");
    Change2Activejs.className = "active";
</script>


<!-- end Configuration of accepted files -->
<s
<script type="text/javascript" src="js/autocompleteBatch.js"></script>


    <script>
    App.restartGlobalFunction();

    $.getScript('../assets/plugins/chart-js/chart.js').done(function () {
        $.getScript('../assets/js/chart-js.demo.min.js').done(function () {
            ChartJs.init();
        });
    });
    </script>
    <!-- ================== BEGIN PAGE LEVEL JS ================== -->

    <!-- ================== END PAGE LEVEL JS ================== -->
