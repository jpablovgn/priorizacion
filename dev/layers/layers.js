var baseLayer = new ol.layer.Group({
    'title': '',
    layers: [
new ol.layer.Tile({
    'title': 'OSM B&W',
    'type': 'base',
    source: new ol.source.XYZ({
        url: 'http://{a-c}.www.toolserver.org/tiles/bw-mapnik/{z}/{x}/{y}.png',
        attributions: [new ol.Attribution({html: '&copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>'})]
    })
})
]
});
var format_corredor_origen0 = new ol.format.GeoJSON();
var features_corredor_origen0 = format_corredor_origen0.readFeatures(json_corredor_origen0,
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_corredor_origen0 = new ol.source.Vector({
    attributions: [new ol.Attribution({html: '<a href=""></a>'})],
});
jsonSource_corredor_origen0.addFeatures(features_corredor_origen0);var lyr_corredor_origen0 = new ol.layer.Vector({
                source:jsonSource_corredor_origen0,
                style: style_corredor_origen0,
                title: '<img src="styles/legend/corredor_origen0.png" /> corredor_origen'
            });var format_Calle_441 = new ol.format.GeoJSON();
var features_Calle_441 = format_Calle_441.readFeatures(json_Calle_441,
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_Calle_441 = new ol.source.Vector({
    attributions: [new ol.Attribution({html: '<a href=""></a>'})],
});
jsonSource_Calle_441.addFeatures(features_Calle_441);var lyr_Calle_441 = new ol.layer.Vector({
                source:jsonSource_Calle_441,
                style: style_Calle_441,
                title: '<img src="../styles/legend/Calle_441.png" /> Calle_44'
            });var format_462 = new ol.format.GeoJSON();
var features_462 = format_462.readFeatures(json_462,
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_462 = new ol.source.Vector({
    attributions: [new ol.Attribution({html: '<a href=""></a>'})],
});
jsonSource_462.addFeatures(features_462);var lyr_462 = new ol.layer.Vector({
                source:jsonSource_462,
                style: style_462,
                title: '<img src="../styles/legend/462.png" /> 46'
            });var format_Carrera_653 = new ol.format.GeoJSON();
var features_Carrera_653 = format_Carrera_653.readFeatures(json_Carrera_653,
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_Carrera_653 = new ol.source.Vector({
    attributions: [new ol.Attribution({html: '<a href=""></a>'})],
});
jsonSource_Carrera_653.addFeatures(features_Carrera_653);var lyr_Carrera_653 = new ol.layer.Vector({
                source:jsonSource_Carrera_653,
                style: style_Carrera_653,
                title: '<img src="../styles/legend/Carrera_653.png" /> Carrera_65'
            });var format_Colombia4 = new ol.format.GeoJSON();
var features_Colombia4 = format_Colombia4.readFeatures(json_Colombia4,
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_Colombia4 = new ol.source.Vector({
    attributions: [new ol.Attribution({html: '<a href=""></a>'})],
});
jsonSource_Colombia4.addFeatures(features_Colombia4);var lyr_Colombia4 = new ol.layer.Vector({
                source:jsonSource_Colombia4,
                style: style_Colombia4,
                title: '<img src="../styles/legend/Colombia4.png" /> Colombia'
            });var format_Carrera43A5 = new ol.format.GeoJSON();
var features_Carrera43A5 = format_Carrera43A5.readFeatures(json_Carrera43A5,
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_Carrera43A5 = new ol.source.Vector({
    attributions: [new ol.Attribution({html: '<a href=""></a>'})],
});
jsonSource_Carrera43A5.addFeatures(features_Carrera43A5);var lyr_Carrera43A5 = new ol.layer.Vector({
                source:jsonSource_Carrera43A5,
                style: style_Carrera43A5,
                title: '<img src="../styles/legend/Carrera43A5.png" /> Carrera43A'
            });var format_Carrera_65_Bolivariana6 = new ol.format.GeoJSON();
var features_Carrera_65_Bolivariana6 = format_Carrera_65_Bolivariana6.readFeatures(json_Carrera_65_Bolivariana6,
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_Carrera_65_Bolivariana6 = new ol.source.Vector({
    attributions: [new ol.Attribution({html: '<a href=""></a>'})],
});
jsonSource_Carrera_65_Bolivariana6.addFeatures(features_Carrera_65_Bolivariana6);var lyr_Carrera_65_Bolivariana6 = new ol.layer.Vector({
                source:jsonSource_Carrera_65_Bolivariana6,
                style: style_Carrera_65_Bolivariana6,
                title: '<img src="../styles/legend/Carrera_65_Bolivariana6.png" /> Carrera_65_Bolivariana'
            });var format_Oriental_Poblado7 = new ol.format.GeoJSON();
var features_Oriental_Poblado7 = format_Oriental_Poblado7.readFeatures(json_Oriental_Poblado7,
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_Oriental_Poblado7 = new ol.source.Vector({
    attributions: [new ol.Attribution({html: '<a href=""></a>'})],
});
jsonSource_Oriental_Poblado7.addFeatures(features_Oriental_Poblado7);var lyr_Oriental_Poblado7 = new ol.layer.Vector({
                source:jsonSource_Oriental_Poblado7,
                style: style_Oriental_Poblado7,
                title: '<img src="../styles/legend/Oriental_Poblado7.png" /> Oriental_Poblado'
            });var format_corredor_destino8 = new ol.format.GeoJSON();
var features_corredor_destino8 = format_corredor_destino8.readFeatures(json_corredor_destino8,
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_corredor_destino8 = new ol.source.Vector({
    attributions: [new ol.Attribution({html: '<a href=""></a>'})],
});
jsonSource_corredor_destino8.addFeatures(features_corredor_destino8);var lyr_corredor_destino8 = new ol.layer.Vector({
                source:jsonSource_corredor_destino8,
                style: style_corredor_destino8,
                title: '<img src="../styles/legend/corredor_destino8.png" /> corredor_destino'
            });var format_Calle_449 = new ol.format.GeoJSON();
var features_Calle_449 = format_Calle_449.readFeatures(json_Calle_449,
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_Calle_449 = new ol.source.Vector({
    attributions: [new ol.Attribution({html: '<a href=""></a>'})],
});
jsonSource_Calle_449.addFeatures(features_Calle_449);var lyr_Calle_449 = new ol.layer.Vector({
                source:jsonSource_Calle_449,
                style: style_Calle_449,
                title: '<img src="../styles/legend/Calle_449.png" /> Calle_44'
            });var format_Carrera_6510 = new ol.format.GeoJSON();
var features_Carrera_6510 = format_Carrera_6510.readFeatures(json_Carrera_6510,
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_Carrera_6510 = new ol.source.Vector({
    attributions: [new ol.Attribution({html: '<a href=""></a>'})],
});
jsonSource_Carrera_6510.addFeatures(features_Carrera_6510);var lyr_Carrera_6510 = new ol.layer.Vector({
                source:jsonSource_Carrera_6510,
                style: style_Carrera_6510,
                title: '<img src="../styles/legend/Carrera_6510.png" /> Carrera_65'
            });var format_Carrera4611 = new ol.format.GeoJSON();
var features_Carrera4611 = format_Carrera4611.readFeatures(json_Carrera4611,
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_Carrera4611 = new ol.source.Vector({
    attributions: [new ol.Attribution({html: '<a href=""></a>'})],
});
jsonSource_Carrera4611.addFeatures(features_Carrera4611);var lyr_Carrera4611 = new ol.layer.Vector({
                source:jsonSource_Carrera4611,
                style: style_Carrera4611,
                title: '<img src="../styles/legend/Carrera4611.png" /> Carrera 46'
            });var format_colombia12 = new ol.format.GeoJSON();
var features_colombia12 = format_colombia12.readFeatures(json_colombia12,
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_colombia12 = new ol.source.Vector({
    attributions: [new ol.Attribution({html: '<a href=""></a>'})],
});
jsonSource_colombia12.addFeatures(features_colombia12);var lyr_colombia12 = new ol.layer.Vector({
                source:jsonSource_colombia12,
                style: style_colombia12,
                title: '<img src="../styles/legend/colombia12.png" /> colombia'
            });var format_Cr43A13 = new ol.format.GeoJSON();
var features_Cr43A13 = format_Cr43A13.readFeatures(json_Cr43A13,
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_Cr43A13 = new ol.source.Vector({
    attributions: [new ol.Attribution({html: '<a href=""></a>'})],
});
jsonSource_Cr43A13.addFeatures(features_Cr43A13);var lyr_Cr43A13 = new ol.layer.Vector({
                source:jsonSource_Cr43A13,
                style: style_Cr43A13,
                title: '<img src="../styles/legend/Cr43A13.png" /> Cr 43 A'
            });var format_Cr65Bolivariana14 = new ol.format.GeoJSON();
var features_Cr65Bolivariana14 = format_Cr65Bolivariana14.readFeatures(json_Cr65Bolivariana14,
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_Cr65Bolivariana14 = new ol.source.Vector({
    attributions: [new ol.Attribution({html: '<a href=""></a>'})],
});
jsonSource_Cr65Bolivariana14.addFeatures(features_Cr65Bolivariana14);var lyr_Cr65Bolivariana14 = new ol.layer.Vector({
                source:jsonSource_Cr65Bolivariana14,
                style: style_Cr65Bolivariana14,
                title: '<img src="../styles/legend/Cr65Bolivariana14.png" /> Cr65 Bolivariana'
            });var format_Oriental_Poblado15 = new ol.format.GeoJSON();
var features_Oriental_Poblado15 = format_Oriental_Poblado15.readFeatures(json_Oriental_Poblado15,
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_Oriental_Poblado15 = new ol.source.Vector({
    attributions: [new ol.Attribution({html: '<a href=""></a>'})],
});
jsonSource_Oriental_Poblado15.addFeatures(features_Oriental_Poblado15);var lyr_Oriental_Poblado15 = new ol.layer.Vector({
                source:jsonSource_Oriental_Poblado15,
                style: style_Oriental_Poblado15,
                title: '<img src="../styles/legend/Oriental_Poblado15.png" /> Oriental_Poblado'
            });

lyr_corredor_origen0.setVisible(true);lyr_Calle_441.setVisible(true);lyr_462.setVisible(true);lyr_Carrera_653.setVisible(true);lyr_Colombia4.setVisible(true);lyr_Carrera43A5.setVisible(true);lyr_Carrera_65_Bolivariana6.setVisible(true);lyr_Oriental_Poblado7.setVisible(true);lyr_corredor_destino8.setVisible(true);lyr_Calle_449.setVisible(true);lyr_Carrera_6510.setVisible(true);lyr_Carrera4611.setVisible(true);lyr_colombia12.setVisible(true);lyr_Cr43A13.setVisible(true);lyr_Cr65Bolivariana14.setVisible(true);lyr_Oriental_Poblado15.setVisible(true);
var layersList = [baseLayer,lyr_corredor_origen0,lyr_Calle_441,lyr_462,lyr_Carrera_653,lyr_Colombia4,lyr_Carrera43A5,lyr_Carrera_65_Bolivariana6,lyr_Oriental_Poblado7,lyr_corredor_destino8,lyr_Calle_449,lyr_Carrera_6510,lyr_Carrera4611,lyr_colombia12,lyr_Cr43A13,lyr_Cr65Bolivariana14,lyr_Oriental_Poblado15];
lyr_corredor_origen0.set('fieldAliases', {'NOMBRE': 'NOMBRE', 'IP_ORIGEN': 'IP_ORIGEN', 'LATITUD_ORIGEN': 'LATITUD_ORIGEN', 'LONGITUD_ORIGEN': 'LONGITUD_ORIGEN', });
lyr_Calle_441.set('fieldAliases', {'NOMBRE': 'NOMBRE', 'IP': 'IP', 'LATITUD': 'LATITUD', 'LONGITUD': 'LONGITUD', });
lyr_462.set('fieldAliases', {'NOMBRE': 'NOMBRE', 'IP': 'IP', 'LATITUD': 'LATITUD', 'LONGITUD': 'LONGITUD', });
lyr_Carrera_653.set('fieldAliases', {'NOMBRE': 'NOMBRE', 'IP': 'IP', 'LATITUD': 'LATITUD', 'LONGITUD': 'LONGITUD', });
lyr_Colombia4.set('fieldAliases', {'NOMBRE': 'NOMBRE', 'IP': 'IP', 'LATITUD': 'LATITUD', 'LONGITUD': 'LONGITUD', });
lyr_Carrera43A5.set('fieldAliases', {'NOMBRE': 'NOMBRE', 'IP': 'IP', 'LATITUD': 'LATITUD', 'LONGITUD': 'LONGITUD', });
lyr_Carrera_65_Bolivariana6.set('fieldAliases', {'NOMBRE': 'NOMBRE', 'IP': 'IP', 'LATITUD': 'LATITUD', 'LONGITUD': 'LONGITUD', });
lyr_Oriental_Poblado7.set('fieldAliases', {'NOMBRE': 'NOMBRE', 'IP': 'IP', 'LATITUD': 'LATITUD', 'LONGITUD': 'LONGITUD', });
lyr_corredor_destino8.set('fieldAliases', {'NOMBRE': 'NOMBRE', 'IP_DESTINO': 'IP_DESTINO', 'LATITUD_DESTINO': 'LATITUD_DESTINO', 'LONGITUD_DESTINO': 'LONGITUD_DESTINO', });
lyr_Calle_449.set('fieldAliases', {'NOMBRE': 'NOMBRE', 'IP': 'IP', 'LATITUD': 'LATITUD', 'LONGITUD': 'LONGITUD', });
lyr_Carrera_6510.set('fieldAliases', {'NOMBRE': 'NOMBRE', 'IP': 'IP', 'LATITUD': 'LATITUD', 'LONGITUD': 'LONGITUD', });
lyr_Carrera4611.set('fieldAliases', {'NOMBRE': 'NOMBRE', 'IP': 'IP', 'LATITUD': 'LATITUD', 'LONGITUD': 'LONGITUD', });
lyr_colombia12.set('fieldAliases', {'NOMBRE': 'NOMBRE', 'IP': 'IP', 'LATITUD': 'LATITUD', 'LONGITUD': 'LONGITUD', });
lyr_Cr43A13.set('fieldAliases', {'NOMBRE': 'NOMBRE', 'IP': 'IP', 'LATITUD': 'LATITUD', 'LONGITUD': 'LONGITUD', });
lyr_Cr65Bolivariana14.set('fieldAliases', {'NOMBRE': 'NOMBRE', 'IP': 'IP', 'LATITUD': 'LATITUD', 'LONGITUD': 'LONGITUD', });
lyr_Oriental_Poblado15.set('fieldAliases', {'NOMBRE': 'NOMBRE', 'IP': 'IP', 'LATITUD': 'LATITUD', 'LONGITUD': 'LONGITUD', });
lyr_corredor_origen0.set('fieldImages', {'NOMBRE': 'TextEdit', 'IP_ORIGEN': 'TextEdit', 'LATITUD_ORIGEN': 'TextEdit', 'LONGITUD_ORIGEN': 'TextEdit', });
lyr_Calle_441.set('fieldImages', {'NOMBRE': 'TextEdit', 'IP': 'TextEdit', 'LATITUD': 'TextEdit', 'LONGITUD': 'TextEdit', });
lyr_462.set('fieldImages', {'NOMBRE': 'TextEdit', 'IP': 'TextEdit', 'LATITUD': 'TextEdit', 'LONGITUD': 'TextEdit', });
lyr_Carrera_653.set('fieldImages', {'NOMBRE': 'TextEdit', 'IP': 'TextEdit', 'LATITUD': 'TextEdit', 'LONGITUD': 'TextEdit', });
lyr_Colombia4.set('fieldImages', {'NOMBRE': 'TextEdit', 'IP': 'TextEdit', 'LATITUD': 'TextEdit', 'LONGITUD': 'TextEdit', });
lyr_Carrera43A5.set('fieldImages', {'NOMBRE': 'TextEdit', 'IP': 'TextEdit', 'LATITUD': 'TextEdit', 'LONGITUD': 'TextEdit', });
lyr_Carrera_65_Bolivariana6.set('fieldImages', {'NOMBRE': 'TextEdit', 'IP': 'TextEdit', 'LATITUD': 'TextEdit', 'LONGITUD': 'TextEdit', });
lyr_Oriental_Poblado7.set('fieldImages', {'NOMBRE': 'TextEdit', 'IP': 'TextEdit', 'LATITUD': 'TextEdit', 'LONGITUD': 'TextEdit', });
lyr_corredor_destino8.set('fieldImages', {'NOMBRE': 'TextEdit', 'IP_DESTINO': 'TextEdit', 'LATITUD_DESTINO': 'TextEdit', 'LONGITUD_DESTINO': 'TextEdit', });
lyr_Calle_449.set('fieldImages', {'NOMBRE': 'TextEdit', 'IP': 'TextEdit', 'LATITUD': 'TextEdit', 'LONGITUD': 'TextEdit', });
lyr_Carrera_6510.set('fieldImages', {'NOMBRE': 'TextEdit', 'IP': 'TextEdit', 'LATITUD': 'TextEdit', 'LONGITUD': 'TextEdit', });
lyr_Carrera4611.set('fieldImages', {'NOMBRE': 'TextEdit', 'IP': 'TextEdit', 'LATITUD': 'TextEdit', 'LONGITUD': 'TextEdit', });
lyr_colombia12.set('fieldImages', {'NOMBRE': 'TextEdit', 'IP': 'TextEdit', 'LATITUD': 'TextEdit', 'LONGITUD': 'TextEdit', });
lyr_Cr43A13.set('fieldImages', {'NOMBRE': 'TextEdit', 'IP': 'TextEdit', 'LATITUD': 'TextEdit', 'LONGITUD': 'TextEdit', });
lyr_Cr65Bolivariana14.set('fieldImages', {'NOMBRE': 'TextEdit', 'IP': 'TextEdit', 'LATITUD': 'TextEdit', 'LONGITUD': 'TextEdit', });
lyr_Oriental_Poblado15.set('fieldImages', {'NOMBRE': 'TextEdit', 'IP': 'TextEdit', 'LATITUD': 'TextEdit', 'LONGITUD': 'TextEdit', });
lyr_corredor_origen0.set('fieldLabels', {'NOMBRE': 'no label', 'IP_ORIGEN': 'no label', 'LATITUD_ORIGEN': 'no label', 'LONGITUD_ORIGEN': 'no label', });
lyr_Calle_441.set('fieldLabels', {'NOMBRE': 'no label', 'IP': 'no label', 'LATITUD': 'no label', 'LONGITUD': 'no label', });
lyr_462.set('fieldLabels', {'NOMBRE': 'no label', 'IP': 'no label', 'LATITUD': 'no label', 'LONGITUD': 'no label', });
lyr_Carrera_653.set('fieldLabels', {'NOMBRE': 'no label', 'IP': 'no label', 'LATITUD': 'no label', 'LONGITUD': 'no label', });
lyr_Colombia4.set('fieldLabels', {'NOMBRE': 'no label', 'IP': 'no label', 'LATITUD': 'no label', 'LONGITUD': 'no label', });
lyr_Carrera43A5.set('fieldLabels', {'NOMBRE': 'no label', 'IP': 'no label', 'LATITUD': 'no label', 'LONGITUD': 'no label', });
lyr_Carrera_65_Bolivariana6.set('fieldLabels', {'NOMBRE': 'no label', 'IP': 'no label', 'LATITUD': 'no label', 'LONGITUD': 'no label', });
lyr_Oriental_Poblado7.set('fieldLabels', {'NOMBRE': 'no label', 'IP': 'no label', 'LATITUD': 'no label', 'LONGITUD': 'no label', });
lyr_corredor_destino8.set('fieldLabels', {'NOMBRE': 'no label', 'IP_DESTINO': 'no label', 'LATITUD_DESTINO': 'no label', 'LONGITUD_DESTINO': 'no label', });
lyr_Calle_449.set('fieldLabels', {'NOMBRE': 'no label', 'IP': 'no label', 'LATITUD': 'no label', 'LONGITUD': 'no label', });
lyr_Carrera_6510.set('fieldLabels', {'NOMBRE': 'no label', 'IP': 'no label', 'LATITUD': 'no label', 'LONGITUD': 'no label', });
lyr_Carrera4611.set('fieldLabels', {'NOMBRE': 'no label', 'IP': 'no label', 'LATITUD': 'no label', 'LONGITUD': 'no label', });
lyr_colombia12.set('fieldLabels', {'NOMBRE': 'no label', 'IP': 'no label', 'LATITUD': 'no label', 'LONGITUD': 'no label', });
lyr_Cr43A13.set('fieldLabels', {'NOMBRE': 'no label', 'IP': 'no label', 'LATITUD': 'no label', 'LONGITUD': 'no label', });
lyr_Cr65Bolivariana14.set('fieldLabels', {'NOMBRE': 'no label', 'IP': 'no label', 'LATITUD': 'no label', 'LONGITUD': 'no label', });
lyr_Oriental_Poblado15.set('fieldLabels', {'NOMBRE': 'no label', 'IP': 'no label', 'LATITUD': 'no label', 'LONGITUD': 'no label', });
lyr_Oriental_Poblado15.on('precompose', function(evt) {
    evt.context.globalCompositeOperation = 'normal';
});
