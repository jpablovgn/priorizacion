FusionCharts.ready(function () {
    var revenueChart = new FusionCharts({
        type: 'pareto2d',
        renderAt: 'chart3-container',
        width: '400',
        height: '280',
        dataFormat: 'json',
        dataSource: {            
            "chart": {
                "caption": "Uso de la APP",
                "subCaption": "Año 2017",
                "paletteColors": "#0075c2",
                "lineColor": "#1aaf5d",
                "xAxisName": "Causas Detectadas",
                "pYAxisName": "No. de Ocurrencias",
               
                "bgColor": "#ffffff",
                "borderAlpha": "20",
                "showCanvasBorder": "0",
                "showHoverEffect": "1",
                "usePlotGradientColor": "0",
                "plotBorderAlpha": "10",
                "showValues": "0",                
                "showXAxisLine": "1",
                "xAxisLineColor": "#999999",
                "divlineColor": "#999999",               
                "divLineIsDashed": "1",
                "showAlternateHGridColor": "0",
                "subcaptionFontBold": "0",
                "subcaptionFontSize": "12"
            },
            "data": [
                {
                    "label": "Tráfico",
                    "value": "280"
                },
                {
                    "label": "Daños Vehículo",
                    "value": "036"
                },
                {
                    "label": "Detención en la vía",
                    "value": "50"
                },
                {
                    "label": "Clima",
                    "value": "50"
                },
                {
                    "label": "Desvío de Ruta",
                    "value": "40"
                },
                {
                    "label": "Otros",
                    "value": "68"
                }
            ]
        }
    }).render();    
});