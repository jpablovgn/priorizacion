<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$url = 'http://192.168.50.187/spsa/heatmap.csv';
function download_file()
{
    // getting content to use in the example
    $content = file_get_contents($url);
    $filename = '/tmp/heatmap.csv';

    // creating temp file
    $handle = fopen($filename, 'w+');
    fwrite($handle, $content);
    fclose($handle);

    // setting headers
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename="'.basename($filename).'"');
    header('Content-Length: ' . filesize($filename));
    return readfile($filename);
}

if (isset($_GET['export']) && $_GET['export'] == '1') {
    
    download_file();
    exit();
}

?>
<a href="?export=1">Export</a>
