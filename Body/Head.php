<!DOCTYPE html>
<!--[if IE 8]> <html lang="es" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="es">
    <!--<![endif]-->
    <head>
        <meta http-equiv="Content-type" content="text/html" charset="utf-8" />
        <title>Sistema de Priorización</title>
        <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <script src="https://unpkg.com/leaflet@1.0.2/dist/leaflet.js"></script>
        <link rel="stylesheet" href="https://unpkg.com/leaflet@1.0.2/dist/leaflet.css" />
<!--        <link rel="stylesheet" href="http://cdn.leafletjs.com/leaflet/v0.7.7/leaflet.css" />-->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/r/dt/jq-2.1.4,jszip-2.5.0,pdfmake-0.1.18,dt-1.10.9,af-2.0.0,b-1.0.3,b-colvis-1.0.3,b-html5-1.0.3,b-print-1.0.3,se-1.0.1/datatables.min.css"/>
  <link rel="stylesheet" type="text/css" href="css/download.css"/>
                <link rel="stylesheet" href="awesome/src/leaflet_awesome_number_markers.css" />

<link rel="stylesheet" href=" https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css "/>
<link rel="stylesheet" href=" https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css "/>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="initial-scale=1,user-scalable=no,maximum-scale=1,width=device-width">
        <meta name="mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <link rel="stylesheet" href="css/leaflet.css" /><link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css"><link rel="stylesheet" href="css/L.Control.Locate.min.css" />
        <link rel="stylesheet" type="text/css" href="css/qgis2web.css">
        <link rel="stylesheet" href="css/MarkerCluster.css" />
        <link rel="stylesheet" href="css/MarkerCluster.Default.css" />
        <link rel="stylesheet" href="http://k4r573n.github.io/leaflet-control-osm-geocoder/Control.OSMGeocoder.css" />
        <link rel="stylesheet" href="css/leaflet.draw.css" />

        <link rel="stylesheet" href="css/leaflet.measurecontrol.css" />
        <link rel='stylesheet' href='//api.tiles.mapbox.com/mapbox.js/plugins/leaflet-draw/v0.2.2/leaflet.draw.css' />
         <link rel="stylesheet" href="fullscreen/Control.FullScreen.css" />


         

        <style>
            #map {
                width: 600px;
                height: 481px;
            }
            #delete, #export {
                position: absolute;
                top:50px;
                right:10px;
                z-index:100;
                background:white;
                color:black;
                padding:6px;
                border-radius:4px;
                font-family: 'Helvetica Neue';
                cursor: pointer;
                font-size:12px;
                text-decoration:none;
            }
            #export {
                top:90px;
            }
            #customers {
                font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
                border-collapse: collapse;
                width: 100%;
            }

            #customers td, #customers th {
                border: 1px solid #ddd;
                padding: 8px;
            }

            #customers tr:nth-child(even){background-color: #f2f2f2;}

            #customers tr:hover {background-color: #ddd;}

            #customers th {
                padding-top: 12px;
                padding-bottom: 12px;
                text-align: left;
                background-color: #4CAF50;
                color: white;
            }


            .tooltip {
                position: relative;
                display: inline-block;
                border-bottom: 1px dotted black;
            }

            .tooltip .tooltiptext {
                visibility: hidden;
                width: 120px;
                background-color: black;
                color: #fff;
                text-align: center;
                border-radius: 6px;
                padding: 5px 0;

                /* Position the tooltip */
                position: absolute;
                z-index: 1;
            }

            .tooltip:hover .tooltiptext {
                visibility: visible;
            }
        </style>

        <title></title>


        <!-- Reference the files as been on root directory and add php echo $lvlroot -->
        <!-- ================== BEGIN BASE CSS STYLE ================== -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
        <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
        <link href="<?php echo $lvlroot; ?>assets/plugins/jquery-ui/themes/base/minified/jquery-ui.min.css" rel="stylesheet" />
        <link href="<?php echo $lvlroot; ?>assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
        <link href="<?php echo $lvlroot; ?>assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
        <link href="<?php echo $lvlroot; ?>assets/css/animate.min.css" rel="stylesheet" />
        <link href="<?php echo $lvlroot; ?>assets/css/style.min.css" rel="stylesheet" />
        <link href="<?php echo $lvlroot; ?>assets/css/style-responsive.min.css" rel="stylesheet" />
        <link href="<?php echo $lvlroot; ?>assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet" />
        <link href="<?php echo $lvlroot; ?>assets/plugins/jquery-tag-it/css/jquery.tagit.css" rel="stylesheet" />
        <link href="<?php echo $lvlroot; ?>assets/css/theme/blue.css" rel="stylesheet" id="theme" />
        <link href="<?php echo $lvlroot; ?>assets/plugins/Hover-master/css/hover-min.css" rel="stylesheet" media="all">
        <link href="<?php echo $lvlroot; ?>assets/plugins/Hover-master/css/hover.css" rel="stylesheet" media="all">
        <!-- ================== END BASE CSS STYLE ================== -->

        <!-- ================== BEGIN PAGE LEVEL STYLE ================== -->
        <link href="<?php echo $lvlroot; ?>assets/plugins/nvd3/build/nv.d3.css" rel="stylesheet" />

        <link href="<?php echo $lvlroot; ?>assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css" rel="stylesheet" />
        <link href="<?php echo $lvlroot; ?>assets/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css" rel="stylesheet" />
        <script src="<?php echo $lvlroot; ?>assets/plugins/DataTables/media/js/jquery.dataTables.js"></script>
        <link href="<?php echo $lvlroot; ?>assets/plugins/bootstrap-combobox/css/bootstrap-combobox.css" rel="stylesheet" />
        <link href="<?php echo $lvlroot; ?>assets/plugins/parsley/src/parsley.css" rel="stylesheet" />
        <link href="<?php echo $lvlroot; ?>assets/plugins/jquery-file-upload/blueimp-gallery/blueimp-gallery.min.css" rel="stylesheet" />
        <link href="<?php echo $lvlroot; ?>assets/plugins/ionicons/css/ionicons.min.css" rel="stylesheet" />
        <link href="<?php echo $lvlroot; ?>assets/plugins/jquery-file-upload/css/jquery.fileupload.css" rel="stylesheet" />
        <link href="<?php echo $lvlroot; ?>assets/plugins/jquery-file-upload/css/jquery.fileupload-ui.css" rel="stylesheet" />
        <link href="<?php echo $lvlroot; ?>assets/plugins/gritter/css/jquery.gritter.css" rel="stylesheet" />
        <link href="<?php echo $lvlroot; ?>assets/plugins/switchery/switchery.min.css" rel="stylesheet" />
        <link href="<?php echo $lvlroot; ?>assets/plugins/powerange/powerange.min.css" rel="stylesheet" />
        <link href="<?php echo $lvlroot; ?>assets/plugins/password-indicator/css/password-indicator.css" rel="stylesheet" />
        <link href="<?php echo $lvlroot; ?>assets/plugins/select2/dist/css/select2.min.css" rel="stylesheet" />
        <link href="<?php echo $lvlroot; ?>assets/plugins/simple-line-icons/simple-line-icons.css" rel="stylesheet">
        <!-- ================== END PAGE LEVEL STYLE ================== -->

        <!-- ================== BEGIN BASE JS ================== -->
        <script src="<?php echo $lvlroot; ?>assets/plugins/pace/pace.min.js"></script>
        <!-- ================== END BASE JS ================== -->
    </head>
