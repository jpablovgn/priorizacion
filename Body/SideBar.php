​<?php

class Conexion {

    function conectar() {
        $conn = pg_connect("user=prioridad password=prioridad.2017 host=192.168.50.187 port=5432 dbname=sps");

        if (!$conn) {
            echo "Error, Problemas al conectar con el servidor";
            exit;
        } else {
            return $conn;
        }
    }

    function consulta($sql = null) {
        $resultado = pg_query(Conexion::conectar(), $sql);
        $fila = array();

        while ($row = pg_fetch_row($resultado)) {
            $fila[] = $row;
        }
        return $fila;
    }

    function num_rows($sql = null) {

        $resultado = pg_num_rows(Conexion::conectar(), $sql);
        return $resultado;
    }

}

$usuario = $_SESSION['usuario'];
$result = Conexion::consulta('SELECT usuario FROM usuarios_sps WHERE usuario=' . $usuario . ';');
if (pg_num_rows($result) > 0) {
    $result = 10;
}
?>
<!-- begin #sidebar -->
<div id="sidebar" class="sidebar" style="background-color:#085C72;">
    <!-- begin sidebar scrollbar -->
    <div data-scrollbar="true" data-height="100%">
        <!-- begin sidebar user -->
        <ul class="nav">
            <li class="nav-profile">
                <a href="javascript:;"><img src="<?php echo $lvlroot; ?> alt="" style="display:none;" /></a>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <!--<i class="fa fa-diamond text-primary">    MERCURY</i>-->


            </li>
        </ul>
        <!-- end sidebar user -->
        ​
        <!-- begin sidebar nav -->
        <ul class="nav">
            ​
            <li class="has-sub" id="sidebarHome"> <!-- Home -->
                <a href="<?php echo $lvlroot ?>Home/home.php">
                    <i class="fa fa-2x fa-home"></i>
                    <span>INICIO</span>
                </a>
            </li> <!-- End Home -->

            <li class="has-sub" id="sidebarIngresoMercancia"> 
                <a href="<?php echo $lvlroot ?>Priorizacion/index.php">
                    <i><span class="glyphicon glyphicon-asterisk"></span></i>
                    <span>PRIORIZACIÓN</span>
                </a>

            </li> 

            <li class="has-sub" id="sidebarInformes"> <!-- Informes -->
              <a href="javascript:;">
                    <b class="caret pull-right"></b>
                    <i class="fa fa-info-circle"></i>
                    <span>REPORTES</span>
                </a>
     <ul class="sub-menu">
                    <li id="sidebarReportes">
                        <a href="<?php echo $lvlroot ?>dev/Reportes/index.php">
Reportes                        </a>
                    </li>
                    <li id="sidebarReportesDinamicosDescargables">
                        <a href="<?php echo $lvlroot ?>Reportes/index.php">
Reportes Dinámicos                        </a>
                    </li>

                </ul>
            </li> <!-- End Informes -->

            <li class="has-sub" id="sidebarAdminUsuarios"<?php if ($_SESSION['usuario'] != 'admin') {
    echo 'style="display:none;"';
} ?>  > <!-- Begin Admin Usuarios -->
                <a href="javascript:;">
                    <b class="caret pull-right"></b>
                    <i class="fa fa-users"></i>
                    <span>ADMIN DE USUARIOS</span>
                </a>
                <ul class="sub-menu">
                    <li id="sidebarAdminUsuarios-CrearUsuario">
                        <a href="<?php echo $lvlroot ?>RegistroUsuarios/crearUsuario/index.php">
                            Crear Usuario
                        </a>
                    </li>
                    <li id="sidebarAdminUsuarios-EditarUsuario">
                        <a href="<?php echo $lvlroot ?>RegistroUsuarios/editarUsuario/index.php">
                            Editar Usuario
                        </a>
                    </li>

                </ul>
            </li> <!-- End Usuarios -->

            <li class="has-sub" id="sidebarHome">
                <a href="<?php echo $lvlroot ?>ayuda">
                    <i><span class="glyphicon glyphicon-exclamation-sign"></span></i>
                    <span>AYUDA</span>
                </a>
            </li>

            <li class="has-sub" id="sidebarHome"> 
                <a href="<?php echo $lvlroot ?>logout.php">
                    <i><span class="glyphicon glyphicon-off"></span></i>
                    <span>CERRAR SESIÓN</span>
                </a>
            </li>
            ​
            <br />



            <ul class="logoalcaldiasidebar"><img src="../assets/img/logoalcaldiablanco-01.png"/></ul>
            </li> 
            ​

        </ul>
    </div>
</div>
<div class="sidebar-bg"></div>
