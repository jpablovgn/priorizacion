<?php
session_start();

// The lvlroot variable indicates the levels of direcctories
// the file loaded has to up, to be on the root directory

$lvlroot = "../";
if ($_SESSION['usuario'] == NULL) {
    ?><script>
            window.location = "../Home/exit.php";
    </script><?php
}
// Including Head.
include_once($lvlroot . "Body/Head.php");
include("fusioncharts.php");

// Including Begin Header.
include_once($lvlroot . "Body/BeginPage.php");
//
// Including Side bar.
include_once($lvlroot . "Body/SideBar.php");
?> 
<script type="text/javascript">

    window.setInterval(function () {

        var markers = new L.FeatureGroup();
        var result;
        var url = '../prioridades.php';
        $.getJSON(url, function (result) {

            showmap(result);
        });
        var firefoxIcon = L.icon({
            iconUrl: 'http://cdn.mysitemyway.com/etc-mysitemyway/icons/legacy-previews/icons-256/yellow-comment-bubbles-icons-transport-travel/041632-yellow-comment-bubbles-icon-transport-travel-transportation-school-bus.png',
            iconSize: [54, 62] // size of the icon
        });

        function showmap(result) {
//                            map.removeLayer(markers);

            for (var i = 0; i < result.length; i++) {
                marker = new L.marker([result[i][3], result[i][4]], {icon: firefoxIcon})
                        .bindPopup('<strong>Bus:</strong>' + '' + result[i][2] + '' + '<br><strong>Min_atraso:</strong>' + result[i][0] + '' + '<br><strong>Recorrido:</strong>' + result[i][1]
                                , {showOnMouseOver: true});
                markers.addLayer(marker);

            }
        }

        map.addLayer(markers);
        setTimeout(function () {
            map.removeLayer(markers);
        }, 4000);

    }, 5000);


</script>

<script src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="http://code.jquery.com/jquery-1.8.2.min.js"></script>
<script src="http://cdn.oesmith.co.uk/morris-0.4.1.min.js"></script>
<script src="fusioncharts.js"></script>
<script src="3d.js"></script>
<script src="comparasion.js"></script>
<script src="container.js"></script>
<script src="js/line.js"></script>
<script src="js/circle.js"></script>
<script src="js/leaflet-heatmap.js"></script>

<script type="text/javascript" src="http://static.fusioncharts.com/code/latest/fusioncharts.js"></script>
<script type="text/javascript" src="http://static.fusioncharts.com/code/latest/themes/fusioncharts.theme.fint.js?cacheBust=56"></script>
<script src="areas.js"></script>
<script src="js/leaflet.js"></script><script src="js/L.Control.Locate.min.js"></script>
<script src="js/leaflet-heat.js"></script>
<script src="js/leaflet.rotatedMarker.js"></script>
<script src="js/OSMBuildings-Leaflet.js"></script>
<script src="js/leaflet-hash.js"></script>
<script src="js/Autolinker.min.js"></script>
<script src="http://k4r573n.github.io/leaflet-control-osm-geocoder/Control.OSMGeocoder.js"></script>
<script src="js/leaflet.draw.js"></script>
<script src="js/leaflet.measurecontrol.js"></script>
<script src="js/leaflet.markercluster.js"></script>
<script src="data/EstacionesLinea10.js"></script>
<script src="data/controladoresinfo1.js"></script>
<script src="mapsettings.js"></script>  
<script src="minatraso.js"></script>

<script>
    var lvlrootjs = <?php print json_encode($lvlroot); ?>;
</script>
<!-- begin breadcrumb -->
<!--<ol class="breadcrumb pull-right">
    <li><a href="javascript:;">Inicio</a></li>
    <li><a href="javascript:;">Proceso Bodega</a></li>
    <li class="active">Selección de Unidades</li>
</ol>-->
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse " data-sortable-id="form-plugins-1">
            <div class="panel-heading" style="background: #023141;">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                </div>
                <h4 class="panel-title">
                    <i class="fa fa-upload fa-lg"></i>
                    Controladores                      </h4>
            </div>
            <!-- begin body panel -->
            <div class="panel-body panel-form " id="panel-output-form">
                <div class="form-group col-md-12">
                    <div class="col-md-12">

                        <div id="map" style="width:1280px;height:800px; margin-left:-30px;"></div>
                        <div id='delete'>Eliminar Geometrias</div>
                        <a href='#' id='export'>Exportar Geometrias</a>
                        <script src="js/leaflet.js"></script><script src="js/L.Control.Locate.min.js"></script>
                        <script src="js/leaflet-heat.js"></script>
                        <script src="js/leaflet.rotatedMarker.js"></script>
                        <script src="js/OSMBuildings-Leaflet.js"></script>
                        <script src="js/leaflet-hash.js"></script>
                        <script src="js/Autolinker.min.js"></script>
                        <script src="http://k4r573n.github.io/leaflet-control-osm-geocoder/Control.OSMGeocoder.js"></script>
                        <script src="js/leaflet.draw.js"></script>
                        <script src="js/leaflet.measurecontrol.js"></script>
                        <script src="js/leaflet.markercluster.js"></script>
                        <script src="data/EstacionesLinea10.js"></script>
                        <script src="data/controladoresinfo1.js"></script>
                        <script src="js/leaflet.draw.js"></script>
                        <script>
//    var result;
//    var url = '../prioridades.php';
//    $.getJSON(url, function (result) {
//
//        showmap(result);
//    });
//    var firefoxIcon = L.icon({
//        iconUrl: 'http://cdn.mysitemyway.com/etc-mysitemyway/icons/legacy-previews/icons-256/yellow-comment-bubbles-icons-transport-travel/041632-yellow-comment-bubbles-icon-transport-travel-transportation-school-bus.png',
//        iconSize: [54, 62] // size of the icon
//    });
//    function showmap(result) {
//        for (var i = 0; i < result.length; i++) {
//            marker = new L.marker([result[i][2], result[i][3]], {icon: firefoxIcon})
//                    .bindPopup('<strong>Bus:</strong>' + '' + result[i][1] + '' + '<br><strong>Min_atraso:</strong>' + result[i][0])
//
//                    .addTo(map);
//        }
//
//    }

    var highlightLayer;
    function highlightFeature(e) {
        highlightLayer = e.target;

        if (e.target.feature.geometry.type === 'LineString') {
            highlightLayer.setStyle({
                color: '#ffff00',
            });
        } else {
            highlightLayer.setStyle({
                fillColor: '#ffff00',
                fillOpacity: 1
            });
        }
        highlightLayer.openPopup();
    }
    L.ImageOverlay.include({
        getBounds: function () {
            return this._bounds;
        }
    });
    var map = L.map('map', {
        measureControl: true,
        center: [6.2337, -75.5740],
        zoom: 14,
        zoomControl: true, maxZoom: 28, minZoom: 1
    }).fitBounds([[6.1833961856, -75.676233528], [6.26042346553, -75.5316449712]]);


    var featureGroup = L.featureGroup().addTo(map);

    var drawControl = new L.Control.Draw({
        edit: {
            featureGroup: featureGroup
        }
    }).addTo(map);

    map.on('draw:created', function (e) {



        {
        }// Each time a feaute is created, it's added to the over arching feature group
        featureGroup.addLayer(e.layer);
        addPopup(e.layer);

        var layer = e.layer,
                feature = layer.feature = layer.feature || {}; // Intialize layer.feature

        feature.type = feature.type || "Feature"; // Intialize feature.type
        var props = feature.properties = feature.properties || {}; // Intialize feature.properties
        props.title = "my title";
        props.content = "my content";

        drawnItems.addLayer(layer);

        var type = e.layerType;
        var layer = e.layer;
        var latLngs;

        if (type === 'circle') {
            latLngs = layer.getLatLng();
            alert(latLngs);
        } else {
            alert(latLngs);
        }
    });
    function addPopup(layer) {
        var content = document.createElement("textarea");
        content.addEventListener("keyup", function () {
            layer.feature.properties.desc = content.value;
        });
        layer.on("popupopen", function () {
            content.value = layer.feature.properties.desc;
            content.focus();
        });
        layer.bindPopup(content).openPopup();
    }


    // on click, clear all layers
    document.getElementById('delete').onclick = function (e) {
        featureGroup.clearLayers();
    }

    document.getElementById('export').onclick = function (e) {
        // Extract GeoJson from featureGroup
        var data = featureGroup.toGeoJSON();

        // Stringify the GeoJson
        var convertedData = 'text/json;charset=utf-8,' + encodeURIComponent(JSON.stringify(data));

        // Create export
        document.getElementById('export').setAttribute('href', 'data:' + convertedData);
        document.getElementById('export').setAttribute('download', 'data.geojson');
    }

    var hash = new L.Hash(map);

    map.attributionControl.addAttribution('<a href="https://github.com/tomchadwin/qgis2web" target="_blank"></a>');
    L.control.locate().addTo(map);
    var feature_group = new L.featureGroup([]);
    var bounds_group = new L.featureGroup([]);
    var raster_group = new L.LayerGroup([]);
    var basemap0 = L.tileLayer('http://{s}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="http://openstreetmap.org">OpenStreetMap</a> ,<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a> <a href="http://hot.openstreetmap.org/" target="_blank">Equipo Humanitario OpenStreetMap</a>',
        maxZoom: 28
    });
    basemap0.addTo(map);





    function setBounds() {
    }
    function geoJson2heat(geojson, weight) {
        return geojson.features.map(function (feature) {
            return [
                feature.geometry.coordinates[1],
                feature.geometry.coordinates[0],
                feature.properties[weight]
            ];
        });
    }
    function pop_EstacionesLinea10(feature, layer) {
        layer.on({
            mouseout: function (e) {
                layer.setStyle(style_EstacionesLinea10(feature));

                if (typeof layer.closePopup === 'function') {
                    layer.closePopup();
                } else {
                    layer.eachLayer(function (feature) {
                        feature.closePopup()
                    });
                }
            },
            mouseover: highlightFeature,
        });
        var popupContent = '<table>\
               <tr>\
                   <td colspan="2"><strong>Estaciones</strong><br />' + (feature.properties['Estaciones'] !== null ? Autolinker.link(String(feature.properties['Estaciones'])) : '') + '</td>\
               </tr>\
           </table>';
        layer.bindPopup(popupContent);
    }

    function style_EstacionesLinea10() {
        return {
            pane: 'pane_EstacionesLinea10',
            opacity: 1,
            color: 'rgba(47,71,221,1.0)',
            dashArray: '',
            lineCap: 'round',
            lineJoin: 'round',
            weight: 3.0,
        }
    }
    map.createPane('pane_EstacionesLinea10');
    map.getPane('pane_EstacionesLinea10').style.zIndex = 600;
    map.getPane('pane_EstacionesLinea10').style['mix-blend-mode'] = 'normal';
    var layer_EstacionesLinea10 = new L.geoJson(json_EstacionesLinea10, {
        pane: 'pane_EstacionesLinea10',
        onEachFeature: pop_EstacionesLinea10,
        style: style_EstacionesLinea10
    });
    bounds_group.addLayer(layer_EstacionesLinea10);
    feature_group.addLayer(layer_EstacionesLinea10);
    function pop_controladoresinfo1(feature, layer) {
        layer.on({
            mouseout: function (e) {
                layer.setStyle(style_controladoresinfo1(feature));

                if (typeof layer.closePopup === 'function') {
                    layer.closePopup();
                } else {
                    layer.eachLayer(function (feature) {
                        feature.closePopup()
                    });
                }
            },
            mouseover: highlightFeature,
        });
        var popupContent = '<table>\
               <tr>\
                   <th scope="row">ip</th>\
                   <td>' + (feature.properties['ip'] !== null ? Autolinker.link(String(feature.properties['ip'])) : '') + '</td>\
               </tr>\
               <tr>\
                   <th scope="row">Nombre</th>\
                   <td>' + (feature.properties['Nombre'] !== null ? Autolinker.link(String(feature.properties['Nombre'])) : '') + '</td>\
               </tr>\
               <tr>\
                   <th scope="row">Prioridad</th>\
                   <td>' + (feature.properties['Prioridad'] !== null ? Autolinker.link(String(feature.properties['Prioridad'])) : '') + '</td>\
               </tr>\
               <tr>\
                   <th scope="row">Sentido</th>\
                   <td>' + (feature.properties['Sentido'] !== null ? Autolinker.link(String(feature.properties['Sentido'])) : '') + '</td>\
               </tr>\
               <tr>\
                   <th scope="row">Latitud</th>\
                   <td>' + (feature.properties['Latitud'] !== null ? Autolinker.link(String(feature.properties['Latitud'])) : '') + '</td>\
               </tr>\
               <tr>\
                   <th scope="row">Longitud</th>\
                   <td>' + (feature.properties['Longitud'] !== null ? Autolinker.link(String(feature.properties['Longitud'])) : '') + '</td>\
               </tr>\
           </table>';
        layer.bindPopup(popupContent);
    }

    function style_controladoresinfo1()
    {
        return {
            pane: 'pane_controladoresinfo1',
            radius: 5.6,
            opacity: 1,
            color: 'rgba(0,162,0,1.0)',
            dashArray: '',
            lineCap: 'butt',
            lineJoin: 'miter',
            weight: 2.0,
            fillOpacity: 1,
            fillColor: 'rgba(238,233,246,1.0)',
        }
    }
    map.createPane('pane_controladoresinfo1');
    map.getPane('pane_controladoresinfo1').style.zIndex = 601;
    map.getPane('pane_controladoresinfo1').style['mix-blend-mode'] = 'normal';
    var layer_controladoresinfo1 = new L.geoJson(json_controladoresinfo1, {
        pane: 'pane_controladoresinfo1',
        onEachFeature: pop_controladoresinfo1,
        pointToLayer: function (feature, latlng) {
            return L.circleMarker(latlng, style_controladoresinfo1(feature))
        }
    });
    bounds_group.addLayer(layer_controladoresinfo1);
    feature_group.addLayer(layer_controladoresinfo1);
    raster_group.addTo(map);
    feature_group.addTo(map);
    var osmGeocoder = new L.Control.OSMGeocoder({
        collapsed: false,
        position: 'topright',
        text: 'Search',
    });
    osmGeocoder.addTo(map);
    var baseMaps = {'OSM HOT': basemap0};
    L.control.layers(baseMaps, {'<img src="legend/controladoresinfo1.png" /> controladoresinfo': layer_controladoresinfo1, '<img src="legend/EstacionesLinea10.png" /> EstacionesLinea1': layer_EstacionesLinea10, }, {collapsed: false}).addTo(map);
    setBounds();



                        </script>
                    </div> <br><br>
                    <br>                   
                    <br>
                </div>
                <script type="text/javascript">
                    window.setInterval(function () {

                        var url2 = 'ultimaprioridadregistrada.php';
                        $.getJSON(url2, function (result) {
                            $('#buses').html(result);

                        });

                        var url3 = 'ultimoprioridadtiempo.php';
                        $.getJSON(url3, function (result) {
                            $('#minatraso').html(result);

                        });

                    }, 5000);


                </script>

                <input type="button" class="btn-primary btn" value="Habilitar prioridad" onclick=" ventanaNueva()"/>
                <br>
                <table class="table table-bordered table-striped nowrap" style="width:30%;border-color:#93bf1f;margin-left: 750px;">
                    <tr style="border:2px; height: 3px;" >
                        <td style="background: #93bf1f;">
                            <label class="control-label  text-center" for="message" style="color:#ffffff;">Última Prioridad Registrada</label>
                        </td>
                        <td>
                            <label class="control-label  text-center" id="buses" for="message"></label>

                        </td>
                    </tr>
                    <tr style="border:1px;" >
                        <td style="background:#93bf1f;">
                            <label class="control-label  text-center" for="message" style="color:#ffffff;">Minutos de Atraso</label>
                        </td>
                        <td>
                            <label class="control-label  text-center" id="minatraso" for="message" > </label>

                        </td>
                    </tr>
                </table>
            </div>
            <div class="panel-inverse">
                <div class="panel-heading" style="background: #023141;">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                    <h4 class="panel-title">
                        <i class="fa fa-upload fa-lg"></i>
                        Indicadores                      </h4>
                </div>
                <div class="panel-form panel-body">

                    <div class="form-group col-md-12">
                        <div class="col-md-7">
                            <h3 style="color:#1a5b73;margin-left: 220px;">Últimos Sucesos</h3>     <br> 

                            <table class="table table-bordered table-striped nowrap" id="example" class="display" width="100%"></table>

                        </div>

                        <?php
                        $conn = pg_connect("user=prioridad password=prioridad.2017 host=192.168.50.187 port=5432 dbname=sps");
                        $Hour = "00:00:00";
                        $Hourbefore = date("Y-m-d $Hour");
                        $Hournow = date("Y-m-d H:i:s");
                        
                        $result = pg_query($conn, "select distinct id_bus from apps WHERE fecha_hora BETWEEN '$Hourbefore' AND '$Hournow' AND prioridad_aceptada = 1;");

                        $num = pg_num_rows($result);

                        //$result2 = pg_query($conn, "select distinct id_bus id_bus from apps where id_bus SIMILAR TO 'P-%|A%'");//Total de Buses
//                        $result3 = pg_num_rows($result2);
                        $result3 = 78;
                        $finalresult = ($num / $result3);

                        $porcentajecontroladores = pg_query($conn, "select count(distinct ip) from apps where fecha_hora between '$Hourbefore' AND '$Hournow';");
                        $numerocontroladores = pg_query($conn, "select count(*) from controladores");
                        $porcentajegeocercas = ($porcentajecontroladores / $numerocontroladores);

                        $busesnoprioridad = pg_query($conn, "select count(distinct id_bus) from apps where fecha_hora between '$Hourbefore' AND '$Hournow' AND prioridad=0;");
                        
                        $totalinserccion = pg_query($conn, "select count(*) from controladores;");
                        $interseccionnoregistrando  = pg_query($conn,"(select distinct ip from controladores except all select distinct ip from apps where fecha_hora between '$Hourbefore' AND '$Hournow') union all (select distinct ip from apps where fecha_hora between  '$Hourbefore' AND '$Hournow' except all select distinct ip from controladores);");
                        $resinterseccionregistrando = pg_num_rows($interseccionnoregistrando);

                        
                        ?>

                        <div class="col-md-5">
                            <h3 style="color:#1a5b73;margin-left: 20px;">Resumen del Día</h3>  <br>    

                            <table class="table table-bordered table-striped nowrap" style="width:100%;border-color:#023141">
                                <tr style="border:2px; height: 3px;" >
                                    <td style="background: #023141;">
                                        <label class="control-label  text-center" for="message" style="color:#ffffff;">% Buses que reportan en geocercas</label>
                                    </td>
                                    <td>
                                        <label class="control-label  text-center" id="busesquereportan"  for="message"><?php echo $finalresult * 100; ?></label>

                                    </td>
                                </tr>
                                <tr style="border:1px;" >
                                    <td style="background:#023141;">
                                        <label class="control-label  text-center" for="message" style="color:#ffffff;">% Geocercas que detectaron buses</label>
                                    </td>
                                    <td>
                                        <label class="control-label  text-center" for="message" > -</label>

                                    </td>
                                </tr>
                                <tr style="border:1px;" >
                                    <td style="background:#023141;">
                                        <label class="control-label  text-center" for="message" style="color:#ffffff;"># Intersecciones que no registraron buses</label>
                                    </td>
                                    <td>'
                                        <label class="control-label  text-center" title="<?php echo "43A76"; ?>"for="message" > -</label>
                                    </td>    
                                </tr>
                                <tr style="border:1px;" >
                                    <td style="background:#023141;">
                                        <label class="control-label  text-center" for="message" style="color:#ffffff;"># Promedio de Buses Detectados por Intersección</label>
                                    </td>
                                    <td>
                                        <label class="control-label  text-center" for="message" > - </label>

                                    </td>
                                </tr>
                                <tr style="border:1px;" >
                                    <td style="background:#023141;">
                                        <label class="control-label  text-center" for="message" style="color:#ffffff;"># Buses que no solicitan prioridad</label>
                                    </td>
                                    <td>
                                        <label  class="control-label  text-center" title="<?php echo "A-001,P-102"; ?>"for="message" >-</label>

                                    </td>
                                </tr>
                                <tr style="border:1px;" >
                                    <td style="background:#023141;">
                                        <label class="control-label  text-center" for="message" style="color:#ffffff;">Geocercas que no detectaron Buses ningún día de la semana</label>
                                    </td>
                                    <td>
                                        <label class="control-label  text-center"title="<?php echo "6570"; ?>" for="message" > -</label>

                                    </td>
                                </tr>
                                <tr style="border:1px;" >
                                    <td style="background:#023141;">
                                        <label class="control-label  text-center" for="message" style="color:#ffffff;"># Buses que no se reportaron en ninguna Geocerca</label>
                                    </td>
                                    <td>
                                        <label class="control-label  text-center" title="<?php echo "A-0001"; ?>"for="message" >-</label>

                                    </td>
                                </tr>
                                <tr style="border:1px;" >
                                    <td style="background:#023141;">
                                        <label class="control-label  text-center" for="message" style="color:#ffffff;">Intersección con Mayor Solicitudes </label>
                                    </td>
                                    <td>
                                        <label class="control-label  text-center" title="<?php echo "35A80"; ?>"for="message" > -</label>

                                    </td>
                                </tr>
                                <tr style="border:1px;" >



                                    <td style="background:#023141;">
                                        <label class="control-label  text-center" for="message" style="color:#ffffff;"># Buses Inactivos</label>
                                    </td>
                                    <td>
                                        <label class="control-label  text-center" for="message" > -</label>

                                    </td>
                                </tr>

                            </table>
                        </div>

                    </div>  
                    <br>
                    <div class="form-group col-md-12">
                        <h3 style="color:#1a5b73;margin-left: 300px;">Prioridades Solicitadas</h3>  <br>    

                        <div class="col-md-12">
                            <div id="chart2-container" ></div>
                        </div>
                        <div id='controllers'>
                            <input id='exportpdf' type='Button' value='Exportar a PDF' />&nbsp;&nbsp;
                            <input id='exportsvg' type='Button' value='Exportar a SVG' />&nbsp;&nbsp;
                            <input id='exportpng' type='Button' value='Exportar a PNG' />
                        </div>

                    </div>
                    <br>

                    <!--ggggggggggg -->


                    <hr>

                    <!--                    <div class="form-group col-md-12">
                                            <h3 style="color:#1a5b73;margin-left: 220px;">Minutos de Atraso</h3>     <br> 
                    
                                            <div class="col-md-6">
                                                <table class="table table-bordered table-striped nowrap" id="promedio_minatraso" class="display" width="100%"></table>
                                            </div>
                    
                                            <div class="col-md-6">
                                                <div id="chart-container"></div>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-12">
                                            <div class="col-md-6">
                                                <div id="chart3-container"></div>
                                            </div>
                                            <div class="col-md-6">
                                                <div id="chart21-container"></div>
                                            </div></div>-->


                </div>
            </div>
        </div>        
    </div>
</div>

<script type="text/javascript">

    function ventanaNueva()
    {
        var url = 'habilitarprioridad.php';
        window.open(url, 'Habilitar prioridad', 'width=510, height=370');
    }
    jQuery(function () {
        jQuery('td#cashcalculator_total a').html('XXX');
    });
</script>
<div class="form-group" id="cancel-save" style="display:none;">
    <div class="col-md-12 col-sm-12 text-right">
        <button id="cancelData" name="cancelData" class="btn btn-warning btn-sm">
            <i class="fa fa-ban">Cancelar</i>
        </button>
        <button id="save" name="save" class="btn btn-primary btn-sm">
            <i class="fa fa-floEndppy-o">
                Guardar
            </i>
        </button>
    </div>
</div>


<?php
include_once($lvlroot . "Body/AlertsWindows.php");
?>
</div>
<!-- end row -->
<?php
// Including Js actions, put in the end.
include_once($lvlroot . "Body/JsFoot.php");
// Including End Header.
include_once($lvlroot . "Body/EndPage.php");
// Writing on database.
//include_once("php/Insert2db.php");
?>

<script>
    var lvlrootjs = <?php print json_encode($lvlroot); ?>;
</script>

<!-- // Loading autocomplete handler. -->

<script type="text/javascript" src="js/setValidations.js"></script>
<!-- // Loading validation file. -->
<!--<script type="text/javascript" src="js/validationBatch.js"></script>
<script type="text/javascript">
        //$('#Batch-table').DataTable();
        // Configuring fileupload
        $(function(){
                $('#fileupload').fileupload({
                        maxFileSize: 5242880,
                        maxNumberOfFiles: 1,
                        acceptFileTypes:/(\.|\/)(csv)$/i
                });
        });
</script>-->
<script type="text/javascript">
    // Activating the side bar.
    var Change2Activejs = document.getElementById("sidebarProcesoBodega");
    Change2Activejs.className = "has-sub active";
    var Change2Activejs = document.getElementById("sidebarProcesoBodega-BatchPicking");
    Change2Activejs.className = "active";
</script>


<!-- end Configuration of accepted files -->

<script type="text/javascript">

    var myVar = setInterval(queryI, 5000);


</script>


<script>
    App.restartGlobalFunction();

    $.getScript('../assets/plugins/chart-js/chart.js').done(function () {
        $.getScript('../assets/js/chart-js.demo.min.js').done(function () {
            ChartJs.init();
        });
    });
</script>
<!-- ================== BEGIN PAGE LEVEL JS ================== -->

<!-- ================== END PAGE LEVEL JS ================== -->
