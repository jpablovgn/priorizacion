      var highlightLayer;
        function highlightFeature(e) {
            highlightLayer = e.target;

            if (e.target.feature.geometry.type === 'LineString') {
              highlightLayer.setStyle({
                color: '#ffff00',
              });
            } else {
              highlightLayer.setStyle({
                fillColor: '#ffff00',
                fillOpacity: 1
              });
            }
            highlightLayer.openPopup();
        }
        L.ImageOverlay.include({
            getBounds: function () {
                return this._bounds;
            }
        });
        var map = L.map('map', {
            measureControl:true,
            center: [6.2337,-75.5740],
    zoom: 14,
            zoomControl:true, maxZoom:28, minZoom:1
        }).fitBounds([[6.1833961856,-75.676233528],[6.26042346553,-75.5316449712]]);
        var hash = new L.Hash(map);
        map.attributionControl.addAttribution('<a href="https://github.com/tomchadwin/qgis2web" target="_blank">qgis2web</a>');
        L.control.locate().addTo(map);
        var feature_group = new L.featureGroup([]);
        var bounds_group = new L.featureGroup([]);
        var raster_group = new L.LayerGroup([]);
        var basemap0 = L.tileLayer('http://{s}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors,<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>,Tiles courtesy of <a href="http://hot.openstreetmap.org/" target="_blank">Humanitarian OpenStreetMap Team</a>',
            maxZoom: 28
        });
        basemap0.addTo(map);
        function setBounds() {
        }
        function geoJson2heat(geojson, weight) {
          return geojson.features.map(function(feature) {
            return [
              feature.geometry.coordinates[1],
              feature.geometry.coordinates[0],
              feature.properties[weight]
            ];
          });
        }
        function pop_EstacionesLinea10(feature, layer) {
            layer.on({
                mouseout: function(e) {
                    layer.setStyle(style_EstacionesLinea10(feature));

                    if (typeof layer.closePopup == 'function') {
                        layer.closePopup();
                    } else {
                        layer.eachLayer(function(feature){
                            feature.closePopup()
                        });
                    }
                },
                mouseover: highlightFeature,
            });
            var popupContent = '<table>\
                    <tr>\
                        <td colspan="2"><strong>Estaciones</strong><br />' + (feature.properties['Estaciones'] !== null ? Autolinker.link(String(feature.properties['Estaciones'])) : '') + '</td>\
                    </tr>\
                </table>';
            layer.bindPopup(popupContent);
        }

        function style_EstacionesLinea10() {
            return {
                pane: 'pane_EstacionesLinea10',
                opacity: 1,
                color: 'rgba(47,71,221,1.0)',
                dashArray: '',
                lineCap: 'round',
                lineJoin: 'round',
                weight: 3.0,
            }
        }
        map.createPane('pane_EstacionesLinea10');
        map.getPane('pane_EstacionesLinea10').style.zIndex = 600;
        map.getPane('pane_EstacionesLinea10').style['mix-blend-mode'] = 'normal';
    var layer_EstacionesLinea10 = new L.geoJson(json_EstacionesLinea10, {
        pane: 'pane_EstacionesLinea10',
        onEachFeature: pop_EstacionesLinea10,
        style: style_EstacionesLinea10
    });
        bounds_group.addLayer(layer_EstacionesLinea10);
        feature_group.addLayer(layer_EstacionesLinea10);
        function pop_controladoresinfo1(feature, layer) {
            layer.on({
                mouseout: function(e) {
                    layer.setStyle(style_controladoresinfo1(feature));

                    if (typeof layer.closePopup == 'function') {
                        layer.closePopup();
                    } else {
                        layer.eachLayer(function(feature){
                            feature.closePopup()
                        });
                    }
                },
                mouseover: highlightFeature,
            });
            var popupContent = '<table>\
                    <tr>\
                        <th scope="row">ip</th>\
                        <td>' + (feature.properties['ip'] !== null ? Autolinker.link(String(feature.properties['ip'])) : '') + '</td>\
                    </tr>\
                    <tr>\
                        <th scope="row">Nombre</th>\
                        <td>' + (feature.properties['Nombre'] !== null ? Autolinker.link(String(feature.properties['Nombre'])) : '') + '</td>\
                    </tr>\
                    <tr>\
                        <th scope="row">Prioridad</th>\
                        <td>' + (feature.properties['Prioridad'] !== null ? Autolinker.link(String(feature.properties['Prioridad'])) : '') + '</td>\
                    </tr>\
                    <tr>\
                        <th scope="row">Sentido</th>\
                        <td>' + (feature.properties['Sentido'] !== null ? Autolinker.link(String(feature.properties['Sentido'])) : '') + '</td>\
                    </tr>\
                    <tr>\
                        <th scope="row">Latitud</th>\
                        <td>' + (feature.properties['Latitud'] !== null ? Autolinker.link(String(feature.properties['Latitud'])) : '') + '</td>\
                    </tr>\
                    <tr>\
                        <th scope="row">Longitud</th>\
                        <td>' + (feature.properties['Longitud'] !== null ? Autolinker.link(String(feature.properties['Longitud'])) : '') + '</td>\
                    </tr>\
                </table>';
            layer.bindPopup(popupContent);
        }

        function style_controladoresinfo1() {
            return {
                pane: 'pane_controladoresinfo1',
                radius: 5.6,
                opacity: 1,
                color: 'rgba(0,162,0,1.0)',
                dashArray: '',
                lineCap: 'butt',
                lineJoin: 'miter',
                weight: 2.0,
                fillOpacity: 1,
                fillColor: 'rgba(238,233,246,1.0)',
            }
        }
        map.createPane('pane_controladoresinfo1');
        map.getPane('pane_controladoresinfo1').style.zIndex = 601;
        map.getPane('pane_controladoresinfo1').style['mix-blend-mode'] = 'normal';
        var layer_controladoresinfo1 = new L.geoJson(json_controladoresinfo1, {
            pane: 'pane_controladoresinfo1',
            onEachFeature: pop_controladoresinfo1,
            pointToLayer: function (feature, latlng) {
                return L.circleMarker(latlng, style_controladoresinfo1(feature))
            }
        });
        bounds_group.addLayer(layer_controladoresinfo1);
        feature_group.addLayer(layer_controladoresinfo1);
        raster_group.addTo(map);
        feature_group.addTo(map);
        var osmGeocoder = new L.Control.OSMGeocoder({
            collapsed: false,
            position: 'topright',
            text: 'Search',
        });
        osmGeocoder.addTo(map);
        var baseMaps = {'OSM HOT': basemap0};
        L.control.layers(baseMaps,{'<img src="legend/controladoresinfo1.png" /> controladoresinfo': layer_controladoresinfo1,'<img src="legend/EstacionesLinea10.png" /> EstacionesLinea1': layer_EstacionesLinea10,},{collapsed:false}).addTo(map);
        setBounds();/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


