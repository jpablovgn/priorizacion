<head>
<script src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="http://code.jquery.com/jquery-1.8.2.min.js"></script>
  <script src="fusioncharts.js"></script>
  <script src="container.js"></script>

<meta charset=utf-8 />
</head>

<?php
include("fusioncharts.php");
	// The lvlroot variable indicates the levels of direcctories
	// the file loaded has to up, to be on the root directory
	$lvlroot ="../../";
	// Including Head.
	session_start();
	include($lvlroot."assets/php/start_session.php");
#	include($lvlroot."assets/php/autentication.php");

	include_once($lvlroot."Body/Head.php");
	// Including Begin Header.
	include_once($lvlroot."Body/BeginPage.php");

	// Including Side bar. 
	include_once($lvlroot."Body/SideBar.php");
	include("assets/php/sessions.php");
?>
<script>
	var lvlrootjs = <?php print json_encode($lvlroot); ?>;
</script> 
<div class="row">
    <!-- begin col-12 -->
    <div class="col-md-12">
        <!-- begin panel -->
        <div class="panel panel-inverse" data-sortable-id="form-plugins-1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                </div>
                <h4 class="panel-title">
                    <i class="icon-user-follow fa-2x"></i> 
              	 <?php echo "Tiempos de Viaje " ?>
		   </h4>
		   
            </div>
  
	 <div class="panel-body panel-form"> 
                <form method="post" class="form-horizontal form-bordered" data-parsley-validate="true" name="FormCrearUsuario" id = "FormCrearUsuario">
                    <!-- Start campo nombre -->
                    <div class="form-group">
			        <div class="col-md-6">
          				
      				  </div>
			  
                        <?php

   $columnChart = new FusionCharts("Column2D", "myFirstChart" , 780, 400, "chart-1", "jsonurl", "data.json");

   $columnChart->render();

?>
<div id="chart-1"></div>

<div class="form-group">
<div class="col-md-6">
<div id="chart-container"></div>
</div>
<div class="col-md-6">
<div id="chart2-container"></div>
</div>
</div>
<div class="form-group">
<div class="col-md-6">
<div id="chart3-container"></div>
</div>
<div class="col-md-6">
<div id="chart4-container"></div>
</div>
</div>

                    </div>  

			       </form>
				

            </div>
		
		

</div>
        </div>
        <!-- end panel inverse -->
	  
    </div>
    

<?php	
    // Including Js actions, put in the end.
    include_once($lvlroot."Body/JsFoot.php");
    // Including End Header.
    include_once($lvlroot."Body/EndPage.php");
?>


<script type="text/javascript">
	// Activating the side bar.
	var Change2Activejs = document.getElementById("sidebarHome");
	Change2Activejs.className = "has-sub active";
</script>

<script>
    App.restartGlobalFunction();
    
    $.getScript('../assets/plugins/chart-js/chart.js').done(function() {
        $.getScript('../assets/js/chart-js.demo.min.js').done(function() {
            ChartJs.init();
        });
    });
</script>