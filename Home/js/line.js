FusionCharts.ready(function () {
    var revenueChart = new FusionCharts({
        type: 'pareto2d',
        renderAt: 'chart3-container',
        width: '500',
        height: '380',
        dataFormat: 'json',
        dataSource: {            
            "chart": {
                "caption": "Uso de la APP",
                "paletteColors": "#0075c2",
                "lineColor": "#1aaf5d",
                "showCumulativeLine":0,
                "xAxisName": "Prioridades Solicitadas",
                "pYAxisName": "No. de Ocurrencias",
                          "yaxismaxvalue": "1000",
                "bgColor": "#ffffff",
                "borderAlpha": "20",
                "showCanvasBorder": "0",
                "showHoverEffect": "1",
                "usePlotGradientColor": "0",
                "plotBorderAlpha": "10",
                "showValues": "0",                
                "showXAxisLine": "1",
                "xAxisLineColor": "#999999",
                "divlineColor": "#999999",               
                "divLineIsDashed": "1",
                "showAlternateHGridColor": "0",
                "subcaptionFontBold": "0",
                "subcaptionFontSize": "12"
            },
            "data": [
                {
                    "label": "2017-03-06",
                    "value": "280"
                },
                {
                    "label": "2017-03-07",
                    "value": "236"
                },
                 {
                    "label": "2017-03-08",
                    "value": "276"
                },
                 {
                    "label": "2017-03-09",
                    "value": "256"
                },
                   {
                    "label": "2017-03-10",
                    "value": "296"
                }
//            
            ]
        }
    }).render();    
});