<?php
// The lvlroot variable indicates the levels of direcctories
// the file loaded has to up, to be on the root directory
$lvlroot = "../";
// Including Head.
include_once($lvlroot . "Body/Head.php");
include($lvlroot . "assets/css/datat.css");
// Including Begin Header.
include_once($lvlroot . "Body/BeginPage.php");
//
// Including Side bar.
//include_once($lvlroot . "Body/SideBar.php");
// Including Php database.
?> 


<script>
    var lvlrootjs = <?php print json_encode($lvlroot); ?>;
</script>
<!-- begin breadcrumb -->
<!--<ol class="breadcrumb pull-right">
    <li><a href="javascript:;">Inicio</a></li>
    <li><a href="javascript:;">Proceso Bodega</a></li>
    <li class="active">Selección de Unidades</li>
</ol>-->
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse " data-sortable-id="form-plugins-1">
            <div class="panel-heading" style="background: #023141;">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                </div>
                <h4 class="panel-title">
                    <i class="fa fa-upload fa-lg"></i>
                    Habilitar Prioridades                      </h4>
            </div>
            <!-- begin body panel -->
            <div class="panel-body panel-form " id="panel-output-form">
                <form data-parsley-validate="true" id="formclient" name="formclient" class="form-horizontal form-bordered" method="post" action="">
                    <div class="form-group col-md-12">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="col-md-6 ">

                                    <table class="table table-bordered table-striped nowrap" border=1 color="gray" >
                                        <thead>
                                            <tr style="border: 1px;">
                                                <th  style="font-size:13px;">Seleccionar</th>
                                                <th style="font-size:13px;">Nombre</th>
                                            </tr>
                                         
                                        </thead>
                                        <tbody> 
                                            <?php
                                            $host = '192.168.50.187';
                                            $port = '5432';
                                            $database = 'sps';
                                            $user = 'prioridad';
                                            $password = 'prioridad.2017';

                                            $connectString = 'host=' . $host . ' port=' . $port . ' dbname=' . $database .
                                                    ' user=' . $user . ' password=' . $password;


                                            $link = pg_connect($connectString);
                                            if (!$link) {
                                                die('Error: Could not connect: ' . pg_last_error());
                                            }
                                            $result = pg_query($link, "SELECT nombre,permitir_paso FROM controladores;");
                                            if (!$result) {
                                                echo "An error occured.\n";
                                                exit;
                                            }

                                            while ($row = pg_fetch_assoc($result)) {
                                                ?>

                                                <tr>
                                                    <td><input type="checkbox"  class="chk"value="<?php echo $row['nombre']; ?>" <?php echo $row['permitir_paso'] == 1 ? checked : '' ?> </td>
                                                    <td> <?php echo trim($row['nombre']); ?></td>
                                                </tr>
                                                <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>

                                    <input type="button" class="btn-primary" value="Todos" onclick="SeleccionarTodos()"/>
                                    <input type="button" class="btn-primary" value="Ninguno" onclick="DeSeleccionarTodos()"/>
                                    

                                    <input type="button" class="btn-primary" value="Actualizar" onclick="ActualizarPrioridad()"/>

                                </div>

                            </div>


                        </div>

                    </div>


                </form>
                <br>
            </div>
        </div>




    </div>
</div>


<script type="text/javascript">

    function ActualizarPrioridad()
    {
        var selected = [];
        var selected2 = [];

        var chkArray = [];
        var chkArray2 = [];

        $("input:checkbox:not(:checked)").each(function () {
            chkArray2.push($(this).val);


        });


//        for (var i = 0; i < chkArray2.length; i += 1) {
//            alert("En el índice '" + i + "' hay este valor: " + chkArray2[i]);
//        }

        $(".chk:checked").each(function () {
            chkArray.push($(this).val());
        });


//        $(".chk").each(function () {
//            if ($(this).find(".chk").not(":checked")) {
//
//                alert($(this).val);
//            }
//        });

        var selected;
        selected = chkArray.join(',');
        for (var i = 0; i < chkArray.length; i++) {

            var url = 'updatepriorities.php?nombre=' + chkArray[i];
            
            $.getJSON(url, function (result) {
                if (result)
                {
                    location.reload();
                }

            });
        }

//        var selected2;
//        selected2 = chkArray2.join(',');
//        alert(chkArray2.lenght);
//        for (var i = 0; i < chkArray2.lenght; i++) {
//
//            var url2 = 'updatepriorities2.php?nombre=' + chkArray2[i];
//            alert(url2);
//            $.getJSON(url2, function (result) {
//
//            });
//
//        }





        /* check if there is selected checkboxes, by default the length is 1 as it contains one single comma */
        if (selected.length > 1) {
//            alert("Dispositivo creado con las ip " + selected);
            location.reload(true);

        } else {
            alert("Por favor marca por lo menos un dispositivo para crear el corredor ");
        }


    }

function SeleccionarTodos(){
    $(".chk").prop("checked", true);    
    
}
function DeSeleccionarTodos(){
    $(".chk").prop("checked", false);    
    
}

</script>

<?php
include_once($lvlroot . "Body/AlertsWindows.php");
?>
</div>
<!-- end row -->
<?php
// Including Js actions, put in the end.
include_once($lvlroot . "Body/JsFoot.php");
// Including End Header.
include_once($lvlroot . "Body/EndPage.php");
// Writing on database.
//include_once("php/Insert2db.php");
?>

