

<?php

class Conexion {

    function conectar() {
        $conn = pg_connect("user=prioridad password=prioridad.2017 host=192.168.50.187 port=5432 dbname=sps");

        if (!$conn) {
            echo "Error, Problemas al conectar con el servidor";
            exit;
        } else {
            return $conn;
        }
        
    }

    function consulta($sql = null) {
        $resultado = pg_query(Conexion::conectar(), $sql);
        $fila = array();

        #Para obtener todos los datos debemos iterar en un ciclo, ya que contiene un puntero interno.
        while ($row = pg_fetch_row($resultado)) {
            $fila[] = $row;
                    }
        return $fila;
    }  
}
    
    $result = Conexion::consulta("select apps.id_bus,apps.fecha_hora,controladores.nombre,apps.tiempo_recorrido, apps.min_atraso from apps inner join controladores on apps.ip = controladores.ip order by apps.fecha_hora desc limit 50;");

    print json_encode($result);
