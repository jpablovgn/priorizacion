
<?php
include ("assets/php/postgresqlconnection.php");
session_destroy();?>


<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Sistema de Priorización Semafórica</title>
        <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />

        <!-- ================== BEGIN BASE CSS STYLE ================== -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
        <link href="assets/plugins/jquery-ui/themes/base/minified/jquery-ui.min.css" rel="stylesheet" />
        <link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
        <link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
        <link href="assets/css/animate.min.css" rel="stylesheet" />
        <link href="assets/css/style.min.css" rel="stylesheet" />
        <link href="assets/css/style-responsive.min.css" rel="stylesheet" />
        <link href="assets/css/theme/blue.css" rel="stylesheet" id="theme" />
        <!-- ================== END BASE CSS STYLE ================== -->

        <!-- ================== BEGIN PAGE LEVEL STYLE ================== -->
        <script src="assets/plugins/parsley/src/parsley.css" rel="stylesheet"></script>
        <!-- ================== END PAGE LEVEL STYLE  ================== -->

        <!-- ================== BEGIN BASE JS ================== -->
        <script src="assets/plugins/pace/pace.min.js"></script>
        <!-- ================== END BASE JS ================== -->
    </head>
    <body class="pace-top bg-white">
        <!-- begin #page-loader -->
        <div id="page-loader" class="fade in"><span class="spinner"></span></div>
        <!-- end #page-loader -->

        <!-- begin #page-container -->
        <div id="page-container" class="fade">
            <!-- begin login -->
            <div class="login login-with-news-feed">
                <!-- begin news-feed -->
                <div class="news-feed">
                    <div class="news-image">
                        <img src="assets/img/fondologin-01.jpg" data-id="login-cover-image" alt="" />
                    </div>
                    <div class="news-caption">
                        <h4 class="caption-title"><i class="fa text-primary"></i>BIENVENIDO <?php echo $_SESSION['usuario'];        ?></h4>
                        <p>

                            <b>Es un sistema de gestión semafórica que permite enviar ordenes de cambio, revisar en tiempo real el estado de los controladores semafóricos y correcta atención de solicitudes, administrar usuarios y generar reportes e informes, los cuales pueden ser exportados en archivos compatibles con diferentes programas de análisis de datos
                                Sistema de Priorización Semafórica  desarrollado por </b><a href="http://www.inter-telco.com" target="_blank" class="text-primary"> Inter-Telco S.A.S  <i class="fa fa-wifi"></i>  </a>
                        </p>

                    </div>
                </div>
                <!-- end news-feed -->
                <!-- begin right-content -->
                <div class="right-content">
                    <!-- begin login-header -->
                    <div class="login-header">
                        <div class="brand">
                            <small></small>
                        </div>

                    </div>
                    <!-- end login-header -->
                    <!-- begin login-content -->
                    <div class="login-content">
                        <div class="lineaverde">
                        
                            <form action="validarusuario.php" method="post">
                                Usuario:                                <input type="text" name="usuario" id="usuario" data-parsley-type="alphanum" class="form-control input-lg" data-parsley-length="[3, 15]" placeholder="Nombre de usuario" required/>

                                <br />
                                Password:                                <input type="password" name="password" id="password" class="form-control input-lg" data-parsley-length="[4, 15]" placeholder="Contraseña"  required />

                                <br />
                                <input type="submit" value="Ingresar" class="btn btn-primary btn-block btn-lg"/>
                            </form>
                        </div>
                        <div id="logoalcaldia"><img src="assets/img/logoalcaldialogin-01.png"</div>

                    </div>
                    <!-- end login-content -->
                </div>
                <!-- end right-container -->

            </div>
            <!-- end login -->
        </div>
        <!-- end page container -->

        <!-- ================== BEGIN BASE JS ================== -->
        <script src="assets/plugins/jquery/jquery-1.9.1.min.js"></script>
        <script src="assets/plugins/jquery/jquery-migrate-1.1.0.min.js"></script>
        <script src="assets/plugins/jquery-ui/ui/minified/jquery-ui.min.js"></script>
        <script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/plugins/parsley/dist/parsley.js"></script>

        <!--[if lt IE 9]>
                <script src="assets/crossbrowserjs/html5shiv.js"></script>
                <script src="assets/crossbrowserjs/respond.min.js"></script>
                <script src="assets/crossbrowserjs/excanvas.min.js"></script>
        <![endif]-->
        <script src="assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
        <script src="assets/plugins/jquery-cookie/jquery.cookie.js"></script>
        <script type="text/javascript" src="assets/js/validations.js"></script>

        <!-- ================== END BASE JS ================== -->

        <!-- ================== BEGIN PAGE LEVEL JS ================== -->
        <script src="assets/js/apps.min.js"></script>
        <!-- ================== END PAGE LEVEL JS ================== -->

        <script>
            $(document).ready(function () {
                App.init();
                $('[data-parsley-validate="true"]').parsley();
            });
        </script>

        <script type="text/javascript">

            function fillDriversData()
            {
                var url = 'assets/php/driverData.php';
                var width = '610';
                var height = '470';
                var windowSize = 'width=' + width + ',height=' + height;
                window.open(url, 'Datos del conductor', windowSize);
            }
            ;

        </script>



        <script type="text/javascript">
            function showContent() {
                element = document.getElementById("content");
                check = document.getElementById("check");
                if (check.checked) {
                    element.style.display = 'block';
                } else {
                    element.style.display = 'none';
                }
            }
        </script>


    </body>
</html>

