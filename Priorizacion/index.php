<?php
session_start();

// The lvlroot variable indicates the levels of direcctories
// the file loaded has to up, to be on the root directory
$lvlroot = "../";
if ($_SESSION['usuario'] == NULL) {
    ?><script>
            window.location = "../Home/exit.php";
    </script><?php
}
// Including Head.
include_once($lvlroot . "Body/Head.php");
// Including Begin Header.
include_once($lvlroot . "Body/BeginPage.php");
//
// Including Side bar.
include_once($lvlroot . "Body/SideBar.php");
// Including Php database.
?> 
<script src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="http://code.jquery.com/jquery-1.8.2.min.js"></script>
<script src="http://cdn.oesmith.co.uk/morris-0.4.1.min.js"></script>
<script src="fusioncharts.js"></script>
<script src="3d.js"></script>
<script src="container.js"></script>
<script src="line.js"></script>
<script src="circle.js"></script>
<script src="leaf-heat.js"></script>
<script type="text/javascript">

        $(document).ready(function () {

            prioridadtabla();
        });

        function prioridadtabla() {
            var url = 'priorizaciontable.php';

            $.getJSON(url, function (respuesta) {
                tablapriorizacion(respuesta);


            });
        }


        function tablapriorizacion(respuesta) {

            $('#actuadorespp').DataTable({

                data: respuesta,
                responsive: true,
                destroy: true,

                "language": {
                    "sProcessing": "Procesando...",
                    "sLengthMenu": "Mostrar _MENU_ registros",
                    "sZeroRecords": "No se encontraron resultados",
                    "sEmptyTable": "Ningún dato disponible en esta tabla",
                    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sSearch": "Buscar:",
                    "sUrl": "",
                    "sInfoThousands": ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "Último",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }

                },
                columns: [
                    {title: "IP"},
                    {title: "Prioridad"},
                    {title: "Estrategia"},
                    {title: "Conexiones"},
                    {title: "Nombre"},
                    {title: "Latitud"},
                    {title: "Longitud"}





                ]


            });


        }

</script>

<script>
    var lvlrootjs = <?php print json_encode($lvlroot); ?>;
</script>
<!-- begin breadcrumb -->
<!--<ol class="breadcrumb pull-right">
    <li><a href="javascript:;">Inicio</a></li>
    <li><a href="javascript:;">Proceso Bodega</a></li>
    <li class="active">Selección de Unidades</li>
</ol>-->
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse " data-sortable-id="form-plugins-1">
            <div class="panel-heading" style="background: #023141;">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                </div>
                <h4 class="panel-title">
                    <i class="fa fa-upload fa-lg"></i>
                    Corredores Activos                      </h4>
            </div>
            <!-- begin body panel -->
            <div class="panel-body panel-form " id="panel-output-form">
                <form data-parsley-validate="true" id="formclient" name="formclient" class="form-horizontal form-bordered" method="post" action="">
                    <div class="form-group col-md-12">
                        <div class="col-md-9">

                            <div id="map" style="width:700px;height:800px;"></div>
                            <script src="js/leaflet.js"></script><script src="js/L.Control.Locate.min.js"></script>
                            <script src="js/leaflet-heat.js"></script>
                            <script src="js/leaflet.rotatedMarker.js"></script>
                            <script src="js/OSMBuildings-Leaflet.js"></script>
                            <script src="js/leaflet-hash.js"></script>
                            <script src="js/Autolinker.min.js"></script>
                            <script src="http://k4r573n.github.io/leaflet-control-osm-geocoder/Control.OSMGeocoder.js"></script>
                            <script src="js/leaflet.draw.js"></script>
                            <script src="js/leaflet.measurecontrol.js"></script>
                            <script src="js/leaflet.markercluster.js"></script>
                            <script src="data/EstacionesLinea10.js"></script>
                            <script src="data/controladoresinfo1.js"></script>
                            <script>
    var highlightLayer;
    function highlightFeature(e) {
        highlightLayer = e.target;

        if (e.target.feature.geometry.type === 'LineString') {
            highlightLayer.setStyle({
                color: '#ffff00',
            });
        } else {
            highlightLayer.setStyle({
                fillColor: '#ffff00',
                fillOpacity: 1
            });
        }
        highlightLayer.openPopup();
    }
    L.ImageOverlay.include({
        getBounds: function () {
            return this._bounds;
        }
    });
    var map = L.map('map', {
        measureControl: true,
        center: [6.2337, -75.5740],
        zoom: 13,
        zoomControl: true, maxZoom: 28, minZoom: 1
    }).fitBounds([[6.1833961856, -75.676233528], [6.26042346553, -75.5316449712]]);

    map.on('click', function (e) {
        var lat = e.latlng.lat;
        var long = e.latlng.lng;
        $('#latituddispositivo').val(lat);
        $('#longituddispositivo').val(long);

    });
    var hash = new L.Hash(map);


    map.attributionControl.addAttribution('<a href="https://github.com/tomchadwin/qgis2web" target="_blank"></a>');
    L.control.locate().addTo(map);
    var feature_group = new L.featureGroup([]);
    var bounds_group = new L.featureGroup([]);
    var raster_group = new L.LayerGroup([]);
    var basemap0 = L.tileLayer('http://{s}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="http://openstreetmap.org">OpenStreetMap</a> ,<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a> <a href="http://hot.openstreetmap.org/" target="_blank">Equipo Humanitario OpenStreetMap</a>',
        maxZoom: 28
    });
    basemap0.addTo(map);


//    var result;
//    var url = '../prioridades.php';
//    $.getJSON(url, function (result) {
//
//        showmap(result);
//    });
//    function showmap(result) {
//        for (var i = 0; i < result.length; i++) {
//            marker = new L.marker([result[i][2], result[i][3]])
//                    .bindPopup('<strong>Bus:</strong>' + '' + result[i][1] + '' + '<br><strong>Min_atraso:</strong>' + result[i][0])
//
//                    .addTo(map);
//        }
//
//    }



    function setBounds() {
    }
    function geoJson2heat(geojson, weight) {
        return geojson.features.map(function (feature) {
            return [
                feature.geometry.coordinates[1],
                feature.geometry.coordinates[0],
                feature.properties[weight]
            ];
        });
    }
    function pop_EstacionesLinea10(feature, layer) {
        layer.on({
            mouseout: function (e) {
                layer.setStyle(style_EstacionesLinea10(feature));

                if (typeof layer.closePopup === 'function') {
                    layer.closePopup();
                } else {
                    layer.eachLayer(function (feature) {
                        feature.closePopup()
                    });
                }
            },
            mouseover: highlightFeature,
        });
        var popupContent = '<table>\
                   <tr>\
                       <td colspan="2"><strong>Estaciones</strong><br />' + (feature.properties['Estaciones'] !== null ? Autolinker.link(String(feature.properties['Estaciones'])) : '') + '</td>\
                   </tr>\
               </table>';
        layer.bindPopup(popupContent);
    }

    function style_EstacionesLinea10() {
        return {
            pane: 'pane_EstacionesLinea10',
            opacity: 1,
            color: 'rgba(47,71,221,1.0)',
            dashArray: '',
            lineCap: 'round',
            lineJoin: 'round',
            weight: 3.0,
        }
    }
    map.createPane('pane_EstacionesLinea10');
    map.getPane('pane_EstacionesLinea10').style.zIndex = 600;
    map.getPane('pane_EstacionesLinea10').style['mix-blend-mode'] = 'normal';
    var layer_EstacionesLinea10 = new L.geoJson(json_EstacionesLinea10, {
        pane: 'pane_EstacionesLinea10',
        onEachFeature: pop_EstacionesLinea10,
        style: style_EstacionesLinea10
    });
    bounds_group.addLayer(layer_EstacionesLinea10);
    feature_group.addLayer(layer_EstacionesLinea10);
    function pop_controladoresinfo1(feature, layer) {
        layer.on({
            mouseout: function (e) {
                layer.setStyle(style_controladoresinfo1(feature));

                if (typeof layer.closePopup === 'function') {
                    layer.closePopup();
                } else {
                    layer.eachLayer(function (feature) {
                        feature.closePopup()
                    });
                }
            },
            mouseover: highlightFeature,
        });
        var popupContent = '<table>\
                   <tr>\
                       <th scope="row">ip</th>\
                       <td>' + (feature.properties['ip'] !== null ? Autolinker.link(String(feature.properties['ip'])) : '') + '</td>\
                   </tr>\
                   <tr>\
                       <th scope="row">Nombre</th>\
                       <td>' + (feature.properties['Nombre'] !== null ? Autolinker.link(String(feature.properties['Nombre'])) : '') + '</td>\
                   </tr>\
                   <tr>\
                       <th scope="row">Prioridad</th>\
                       <td>' + (feature.properties['Prioridad'] !== null ? Autolinker.link(String(feature.properties['Prioridad'])) : '') + '</td>\
                   </tr>\
                   <tr>\
                       <th scope="row">Sentido</th>\
                       <td>' + (feature.properties['Sentido'] !== null ? Autolinker.link(String(feature.properties['Sentido'])) : '') + '</td>\
                   </tr>\
                   <tr>\
                       <th scope="row">Latitud</th>\
                       <td>' + (feature.properties['Latitud'] !== null ? Autolinker.link(String(feature.properties['Latitud'])) : '') + '</td>\
                   </tr>\
                   <tr>\
                       <th scope="row">Longitud</th>\
                       <td>' + (feature.properties['Longitud'] !== null ? Autolinker.link(String(feature.properties['Longitud'])) : '') + '</td>\
                   </tr>\
               </table>';
        layer.bindPopup(popupContent);
    }

    function style_controladoresinfo1() {
        return {
            pane: 'pane_controladoresinfo1',
            radius: 5.6,
            opacity: 1,
            color: 'rgba(0,162,0,1.0)',
            dashArray: '',
            lineCap: 'butt',
            lineJoin: 'miter',
            weight: 2.0,
            fillOpacity: 1,
            fillColor: 'rgba(238,233,246,1.0)',
        }
    }
    map.createPane('pane_controladoresinfo1');
    map.getPane('pane_controladoresinfo1').style.zIndex = 601;
    map.getPane('pane_controladoresinfo1').style['mix-blend-mode'] = 'normal';
    var layer_controladoresinfo1 = new L.geoJson(json_controladoresinfo1, {
        pane: 'pane_controladoresinfo1',
        onEachFeature: pop_controladoresinfo1,
        pointToLayer: function (feature, latlng) {
            return L.circleMarker(latlng, style_controladoresinfo1(feature))
        }
    });
    bounds_group.addLayer(layer_controladoresinfo1);
    feature_group.addLayer(layer_controladoresinfo1);
    raster_group.addTo(map);
    feature_group.addTo(map);
    var osmGeocoder = new L.Control.OSMGeocoder({
        collapsed: false,
        position: 'topright',
        text: 'Search',
    });
    osmGeocoder.addTo(map);
    var baseMaps = {'OSM HOT': basemap0};
    L.control.layers(baseMaps, {'<img src="legend/controladoresinfo1.png" /> controladoresinfo': layer_controladoresinfo1, '<img src="legend/EstacionesLinea10.png" /> EstacionesLinea1': layer_EstacionesLinea10, }, {collapsed: false}).addTo(map);
    setBounds();



                            </script>
                        </div>
                        <div class="col-md-3 pull-right">
                            <h3>Actuador</h3>
                            <br>
                            <label>Identificador (IP) (*)</label>

                            <input type="text" class="form-control" id="ipactuador" /><br>
                            <label>Prioridad (*)</label>

                            <select id="prioridadactuador" class="form-control" >
                                <option>Dinámica</option>
                                <option>Absoluta</option>
                                <option>Relativa</option>
                            </select>
                            <br>
                            <label>Estrategia de Control(*)</label>

                            <select id="estrategiaactuador" class="form-control" >
                                <option>COS</option>
                                <option>StandAlone</option>
                                <option>UK</option>
                            </select><br>
                            <label>Conexiones (*)</label>

                            <select id="conexionesactuador" class="form-control" >
                                <option>wifi</option>
                                <option>Ethernet</option>
                                <option>3G,4G</option>
                            </select>                         <br>   <label>Nombre (*)</label>


                            <input type="text" class="form-control" id="nombreactuador"  /><br>
                            <label>Iwnauta (*)</label>


                            <input type="text" class="form-control" id="iwnauta"  /><br>
                            <label>Estado Nauta (*)</label>


                            <input type="text" class="form-control" id="estadonauta"  /><br>
                            <label>Sentido Nauta (*)</label>


                            <input type="text" class="form-control" id="sentidonauta"  /><br>
                            <label>Latitud (*)</label>

                            <input type="text" class="form-control" id="latituddispositivo"  /><br>
                            <label>Longitud (*)</label>

                            <input type="text" class="form-control" id="longituddispositivo"  />
                            <br>
                            <input type ="button" class="btn btn-success"  value="Crear" onclick=" NuevaEstacion()"/>

                            <input type ="button" class="btn btn-success" id="Clean" value="Limpiar"/>


                        </div>
                    </div>


                </form>
                <br>
            </div>
        </div>
        <div class="panel panel-inverse">
            <div class="panel-heading" style="background: #023141;">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                                    <!--<a href="javascript:;" onclick="noSubmit();" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>-->
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                </div>
                <h4 class="panel-title">
                    <i class="icon-beer fa-3x" ></i>
                    Actuador Paso por Prioridad
                </h4>
            </div>
            <div class="panel-body panel-form">

                <!-- Start campo Nombre input desplegable  -->
                <div class ="form-group">
                    <div class="col-md-12">
                        <table class="table table-bordered table-striped nowrap" id="actuadorespp" class="display"width="100%;"></table>

                    </div>
                </div>



            </div>
        </div>

    </div>
    <script>


        var url = 'priorizaciontable.php';
        $.getJSON(url, function (result) {

            tablapriorizacion(result);
        });


        function NuevaEstacion()
        {
            var ip = $('#ipactuador').val();
            var prioridad = $('#prioridadactuador').val();
            var estrategia = $('#estrategiaactuador').val();
            var conexiones = $('#conexionesactuador').val();
            var nombre = $('#nombreactuador').val();
            var iwnauta = $('#iwnauta').val();
            var estado = $('#estadonauta').val();
            var sentido = $('#sentidonauta').val();
            var lat_centro = $('#latituddispositivo').val();
            var lon_centro = $('#longituddispositivo').val();
            var url2 = 'crearActuador.php?ip='+ ip + '&prioridad=' + prioridad + '&estrategia=' + estrategia+'&conexiones=' + conexiones + '&nombre=' + nombre + '&iwnauta=' + iwnauta + '&estado=' + estado + '&sentido=' + sentido + '&latitud=' + lat_centro+ '&longituddispositivo=' + lon_centro;
            alert(url2);
        $.getJSON(url2, function () {
                   $('#ipactuador').val('');
     $('#prioridadactuador').val('');
            $('#estrategiaactuador').val('');
            $('#conexionesactuador').val('');
             $('#nombreactuador').val('');
           $('#iwnauta').val('');
            $('#estadonauta').val('');
            $('#sentidonauta').val('');
            $('#latituddispositivo').val('');
             $('#longituddispositivo').val('');
             location.reload();
            });

        }

    </script>

    <div class="form-group" id="cancel-save" style="display:none;">
        <div class="col-md-12 col-sm-12 text-right">
            <button id="cancelData" name="cancelData" class="btn btn-warning btn-sm">
                <i class="fa fa-ban">Cancelar</i>
            </button>
            <button id="save" name="save" class="btn btn-primary btn-sm">
                <i class="fa fa-floEndppy-o">
                    Guardar
                </i>
            </button>
        </div>
    </div>


    <?php
    include_once($lvlroot . "Body/AlertsWindows.php");
    ?>
</div>
<!-- end row -->
<?php
// Including Js actions, put in the end.
include_once($lvlroot . "Body/JsFoot.php");
// Including End Header.
include_once($lvlroot . "Body/EndPage.php");
// Writing on database.
//include_once("php/Insert2db.php");
?>

<script>
    var lvlrootjs = <?php print json_encode($lvlroot); ?>;
</script>

<!-- // Loading autocomplete handler. -->

<script type="text/javascript" src="js/setValidations.js"></script>
<!-- // Loading validation file. -->
<!--<script type="text/javascript" src="js/validationBatch.js"></script>
<script type="text/javascript">
        //$('#Batch-table').DataTable();
        // Configuring fileupload
        $(function(){
                $('#fileupload').fileupload({
                        maxFileSize: 5242880,
                        maxNumberOfFiles: 1,
                        acceptFileTypes:/(\.|\/)(csv)$/i
                });
        });
</script>-->
<script type="text/javascript">
    // Activating the side bar.
    var Change2Activejs = document.getElementById("sidebarProcesoBodega");
    Change2Activejs.className = "has-sub active";
    var Change2Activejs = document.getElementById("sidebarProcesoBodega-BatchPicking");
    Change2Activejs.className = "active";
</script>


<!-- end Configuration of accepted files -->
<s
<script type="text/javascript" src="js/autocompleteBatch.js"></script>


    <script>
    App.restartGlobalFunction();

    $.getScript('../assets/plugins/chart-js/chart.js').done(function () {
        $.getScript('../assets/js/chart-js.demo.min.js').done(function () {
            ChartJs.init();
        });
    });
    </script>
    <!-- ================== BEGIN PAGE LEVEL JS ================== -->

    <!-- ================== END PAGE LEVEL JS ================== -->
