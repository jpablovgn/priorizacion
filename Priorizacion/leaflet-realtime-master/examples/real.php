<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>
<html>
<head>
    <title>Leaflet Realtime</title>
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.0.3/dist/leaflet.css" />
    <style>
        #map {
            position: absolute;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
        }
    </style>
</head>
<body>
    <div id="map"></div>

    <script src="https://unpkg.com/leaflet@1.0.3/dist/leaflet-src.js"></script>
    <script src="../dist/leaflet-realtime.js"></script>
    <script type="text/javascript">
    var map = L.map('map', {center: [6.2443382,-75.573553], zoom: 8}),
    realtime = L.realtime({
        url: 'prueba3.php',
        crossOrigin: true,
        type: 'json'
    }, {
        interval: 500
    }).addTo(map);
var osm = new L.TileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');
map.addLayer(osm);

function update(e) {
    realtime.update(JSON.parse(e.data));
}

function remove(e) {
    realtime.remove(JSON.parse(e.data));
}
realtime.on('update', function(e) {
        popupContent = function(fId) {
            var feature = e.features[fId],
                my_number = feature.properties.number;
                mystatus = feature.properties.mystatus;
            return ('My number is: '+ my_number + '<br />' + 'Status: ' + mystatus) ;
        },
        bindFeaturePopup =  function(fId) {
            realtime.getLayer(fId).bindPopup(popupContent(fId));
        },
        updateFeaturePopup = function(fId) {
            realtime.getLayer(fId).getPopup().setContent(popupContent(fId));
        };



    Object.keys(e.enter).forEach(bindFeaturePopup);
    Object.keys(e.update).forEach(updateFeaturePopup);
});
    </script>
</body>
</html>